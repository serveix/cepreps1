<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkbook extends Model
{
    public function checks() {
        return $this->hasMany('App\Check');
    }
}
