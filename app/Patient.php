<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $fillable = [
              'name', 'email', 'birthdate', 'birthplace', 'address1', 'address2', 'city', 'state', 'zip_code', 'indications', 'home_phone', 'office_phone', 'celphone', 'referral', 'additional_info'];

    public function servicesCharges() {
    	return $this->hasMany('App\ServicesCharge');
    }

    public function doctorCharges(){
    	return $this->hasMany('App\DoctorCharge');
    }

    public function appointments() {
        return $this->hasMany('App\Appointment');
    }

    public function payments() {
        return $this->hasMany('App\Payment');
    }
}
