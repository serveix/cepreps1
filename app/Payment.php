<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model {
    protected $fillable = [
        'user_id', 'patient_id', 'services_charge_id', 
        'amount', 'payment_method', 'last_four'
    ];

    public function serviceCharge() {
        return $this->belongsTo('App\ServicesCharge');
    }

    public function patient() {
        return $this->belongsTo('App\Patient');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}