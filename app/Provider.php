<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $fillable = [
        'name', 'rfc', 'domicilio', 'telefono'];

    public function servicesCharges() {
        return $this->hasMany('App\ServicesCharge');
    }
}
 