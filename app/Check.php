<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Check extends Model
{
    protected $fillable = ['number', 'amount', 'description', 'invoice_number'];
    
    public function supplier() {
        return $this->belongsTo('App\Supplier');
    }

    public function checkbook() {
        return $this->belongsTo('App\Checkbook');
    }
}
