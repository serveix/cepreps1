<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicesCharge extends Model
{

	protected $fillable = ['user_id', 'patient_id', 'discount_request_id', 'total_amount', 
		'billing_status', 'approval_status', 'patient_paid', 'additional_info'];

	/**
	 * Devuelve el usuario al que pertenece el cargo de servicio
	 */
	public function user() {
		return $this->belongsTo('App\User');
	}

	/**
	 * Devuelve el paciente al cual le pertenece el cargo del servicio
	 */
	public function patient() {
		return $this->belongsTo('App\Patient');
	}

	public function discountRequest() {
		return $this->belongsTo('App\DiscountRequest');
	}

	public function services(){
		return $this->belongsToMany('App\Service')->withPivot('price', 'payment_method', 'last_four');
	}

	public function payments(){
		return $this->hasMany('App\Payment');
	}

	public function provider() {
		return $this->belongsTo('App\Provider');
	}

}
