<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicesDiscount extends Model
{
    protected $fillable = [
        'discount_percentage'
    ];

    public function discountRequest() {
    	return $this->hasMany('App\DiscountRequest');
    }

}
