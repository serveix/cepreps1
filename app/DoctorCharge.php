<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorCharge extends Model
{
	public function user() {
		return $this->belongsTo('App\User');
	}

	public function patient() {
		return $this->belongsTo('App\Patient');
	}

	public function doctor() {
		return $this->belongsTo('App\Doctor');
	}

	public function discountRequest(){
		return $this->belongsTo('App\DiscountRequest');
	}
}
