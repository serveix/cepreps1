<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'day_in', 'hour_in', 'day_out', 'hour_out'
    ];

    public function user() {
    	return $this->belongsTo('App\User');
    }

}
