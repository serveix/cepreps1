<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    protected $fillable = [
        'title', 'description', 'start', 'end', 'color', 'calendar'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function patient() {
        return $this->belongsTo('App\Patient');
    }
}
