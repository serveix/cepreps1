<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Support\Facades\Log;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function logs() {
        return $this->hasMany('App\Log');
    }

    public function servicesCharges() {
        return $this->hasMany('App\ServicesCharge');
    }

    public function doctorCharges() {
        return $this->hasMany('App\DoctorCharge');
    }

    public function appointments() {
        return $this->hasMany('App\Appointment');
    }

    public function payments() {
        return $this->hasMany('App\Payment');
    }

    public function permissions() {
        return $this->belongsToMany('App\Permission');
    }

    public function scopeHasPermission($query, $value) {
        $returnable = $this->permissions()->where('value', $value)->first()===null ? 'no' : 'yes';
        Log::debug($returnable);
        return $returnable;
    }
}