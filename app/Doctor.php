<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $fillable = ['name', 'price'];

    public function doctorCharges() {
    	return $this->hasMany('App\DoctorCharge');
    }
}
