<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscountRequest extends Model
{
    protected $fillable = [
        'approval'
    ];

    public function servicesCharges() {
    	return $this->hasMany('App\ServicesCharge');
    }

    public function doctorCharges() {
    	return $this->hasMany('App\DoctorCharge');
    }

    public function servicesDiscount() {
        return $this->belongsTo('App\ServicesDiscount');
    }
    
}
