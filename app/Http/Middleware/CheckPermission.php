<?php

namespace App\Http\Middleware;

use Closure;

class checkPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        if($request->user()->permissions()->where('value', $permission)->first()===null)
            return redirect('/');

        return $next($request);
    }
}
