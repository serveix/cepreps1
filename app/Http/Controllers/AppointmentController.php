<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Service;
use App\Patient;
use App\User;
use App\Appointment;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class AppointmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * GET: Esta funcion regresa el view par
     * crear nuevas citas. Entrega una instancia pacienta
     * y la fecha y hora de hoy.
     * @param Int
     * @return View
     */
    public function showNewAppointment($id)
    {
        return view('appointments.new')
        ->with('patient', Patient::find($id))
        ->with('rightNow', Carbon::now()->format('d/m/Y h:i A'));
    }

    /**
     * POST: Crea la cita y las relaciones con los datos recibidos.
     * @param Request
     * @param Int
     * @return void
     */
    public function addNewAppointment(Request $request, $id)
    {
        $this->validate($request, [
            'titulo' => 'required|max:190',
            'tiempo' => 'required',
            'color'  => 'required|max:7',
        ]);

        $user           = Auth::user();
        $patient        = Patient::find($id);
        $title          = $request->input('titulo');
        $color          = $request->input('color');
        $datetimeRange  = $request->input('tiempo');
        
        // el datetime en la db debe salir cada uno asi: 2018-02-06 03:15:12
        // hacemos explode() para separar la fecha y hora de inicio y la fecha y hora de termino
        $datetimeLapses    = explode(' - ', $datetimeRange); // resultado: ['00/00/00 00:00 PM', '00/00/00 00:00 AM']    

        // Carbon nos permite manipular fechas muy facilmente
        // asi que convertimos la fecha de String 'd/m/Y h:i A' a Carbon
        $datetimeStart  = Carbon::createFromFormat('d/m/Y h:i A', $datetimeLapses[0] );
        $datetimeEnd    = Carbon::createFromFormat('d/m/Y h:i A', $datetimeLapses[1] );

        $appointment = new Appointment([
            'title' => $title,
            'start' => $datetimeStart->toDateTimeString(),
            'end'   => $datetimeEnd->toDateTimeString(),
            'color' => $color,
        ]);
        $appointment->user()->associate($user);
        $appointment->patient()->associate($patient);
        $appointment->save();

        return back()->with('success', true);
    }

    /**
     * Regresa en formato JSON los datos principales de cada
     * Appointment con el nombre segun los necesita el calendario
     * de la pagina index. (title, start, end, backgroundColor, borderColor)
     * @return JSONArray
     */
    public function getAppointments() {
        $appointments = Appointment::all();
        
        $returnable = collect([]);

        foreach ($appointments as $appointment) {
            $current = [
                'id'              => $appointment->id,
                'title'           => $appointment->title,
                'start'           => $appointment->start,
                'end'             => $appointment->end,
                'backgroundColor' => $appointment->color,
                'borderColor'     => $appointment->color
            ];
            $returnable->push($current);
        }

        return $returnable;
    }

    /**
     * Regresa el HTML de un modal con la informacion del evento para mostrarle al usuario. 
     * Tambien, desde ese modal puede editar o eliminar eventos.
     */
    public function getModal(Appointment $appointment) {
        return view('includes.modals.appointments.main')->with('appointment', $appointment);
    }

    public function deleteOne(Request $request){
        $appointment = Appointment::find($request->input('appointmentId'));
        $appointment->delete();
        return back()->with('success', true);
    }

}
