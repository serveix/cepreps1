<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Patient;

class PatientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    // GET: show list of patients
    public function showNew() {
        return view('patients.new');
    }

	// GET: show list of patients
    public function showList() {
        return view('patients.admin')->with('patients', Patient::all() );
    }

    //POST: adds new patient from form to db
    public function addNew(Request $request) {

        /* Los campos se ponen en español para que cuando
         * el mensaje de error se genere, sea algo como:
         * El campo <campo> es invalido.
         * Ej. El campo nombre es invalido.
         * En vez de:
         * El campo name es invalido.
         */
    	$this->validate($request, [
            'nombre' => 'required|max:190',
            'email' => 'max:190',
            'fecha_nacimiento' => 'required|date',
            'lugar_nacimiento' => 'required|max:190',
            'calle_numero' => 'required|max:190',
            'colonia' => 'required|max:190',
            'ciudad' => 'required|max:190',
            'estado' => 'required|max:190',
            'codigo_postal' => 'required|max:10',
            'indicaciones' => 'max:190',
            'tel_casa' => 'required|max:15',
            'tel_oficina' => 'max:15',
            'celular' => 'max:18',
            'referido' => 'max:190',
            'informacion_adicional' => 'max:190',
        ]);

        $patient = new Patient([
            'name' => $request->input('nombre'),
            'email' => $request->input('email'),
            'birthdate' => $request->input('fecha_nacimiento'),
            'birthplace' => $request->input('lugar_nacimiento'),
            'address1' => $request->input('calle_numero'),
            'address2' => $request->input('colonia'),
            'city' => $request->input('ciudad'),
            'state' => $request->input('estado'),
            'zip_code' => $request->input('codigo_postal'),
            'indications' => $request->input('indicaciones'),
            'home_phone' => $request->input('tel_casa'),
            'office_phone' => $request->input('tel_oficina'),
            'celphone' => $request->input('celular'),
            'referral' => $request->input('referido'),
            'additional_info' => $request->input('informacion_adicional'),
        ]);

        $patient->save();

         return redirect( route('patientDetails', ['id' => $patient->id]) )->with('status', 'Se ha creado el nuevo registro del paciente.');


    }

    public function showOne($id) {
        $textbirthdate = Patient::find($id)->birthdate;
        $arrayb = explode("-", $textbirthdate);

        //Se obtiene el año, mes y dia de nacimiento
        $bornyear = (int)$arrayb[0];
        $bornmonth = (int)$arrayb[1];
        $bornday = (int)$arrayb[2];

        //Se obtiene el año, mes y dia actual
        $year = (int)date('Y');
        $month = (int)date('m');
        $day = (int)date('d');
        
        $y = $year - $bornyear;
        $d = $day - $bornday;
        
        if($d < 0){ 
            $month - 1;
        }
        $m = $month - $bornmonth;
        if($m < 0){
            $y = $y - 1;
            $m = $m + 12;
        }
        
        $age = $y . ' años con ' . $m . ' meses.';
        return view('patients.details')->with('patient', Patient::find($id))->with('age', $age);
    }
 
    public function showOnePrint($id) {
        return view('patients.detailsPrint')->with('patient', Patient::find($id));
    }

    public function editPatient($id)
    {
        return view('Patients.edit')->with('patient', Patient::find($id));
    }

    public function update(Request $request)
    {
        /* Los campos se ponen en español para que cuando
         * el mensaje de error se genere, sea algo como:
         * El campo <campo> es invalido.
         * Ej. El campo nombre es invalido.
         * En vez de:
         * El campo name es invalido.
         */
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|max:190',
            'email' => 'max:190',
            'fecha_nacimiento' => 'required|date',
            'lugar_nacimiento' => 'required|max:190',
            'calle_numero' => 'required|max:190',
            'colonia' => 'required|max:190',
            'ciudad' => 'required|max:190',
            'estado' => 'required|max:190',
            'codigo_postal' => 'required|max:10',
            'indicaciones' => 'max:190',
            'tel_casa' => 'required|max:15',
            'tel_oficina' => 'max:15',
            'celular' => 'max:18',
            'referido' => 'max:190',
            'informacion_adicional' => 'max:190'
        ]);

        if($validator->fails()) {
            return back()->withErrors($validator, 'editPatient')->withInput();
        }

        $patient = Patient::find($request->input('id'));

        $patient->name            = $request->input('nombre');
        $patient->email           = $request->input('email');
        $patient->birthdate       = $request->input('fecha_nacimiento');
        $patient->birthplace      = $request->input('lugar_nacimiento');
        $patient->address1        = $request->input('calle_numero');
        $patient->address2        = $request->input('colonia');
        $patient->city            = $request->input('ciudad');
        $patient->state           = $request->input('estado');
        $patient->zip_code        = $request->input('codigo_postal');
        $patient->indications     = $request->input('indicaciones');
        $patient->home_phone      = $request->input('tel_casa');
        $patient->office_phone    = $request->input('tel_oficina');
        $patient->celphone        = $request->input('celular');
        $patient->referral        = $request->input('referido');
        $patient->additional_info = $request->input('informacion_adicional');

        $patient->save();
        
        return back()->with('editSuccess', true);
    }

    // returns view when search
    public function showPatient($var, $numresults){
        $patients = Patient::where('id', $var)
        ->orWhere('name', 'like', '%' . $var . '%')->orderBy('updated_at', 'desc')->limit($numresults)->get();
        $numresults = count($patients);

        return view('includes.patienttable')->with('patients', $patients)->with('numresults', $numresults);
    }

    public function findPatient()
    {
        return view('patients.search');
    }

    public function findAndShowPatient(Request $request){
        $patient = Patient::find( $request->input('searchedUserId') );
        return view('patients.search')->with('foundPatient', $patient);
    }
}
