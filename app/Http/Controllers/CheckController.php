<?php

namespace App\Http\Controllers;

use App\Check;
use App\Checkbook;
use App\Supplier;
use Illuminate\Http\Request;

class CheckController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // URL Params
        $selectedCheckbook = $request->input('checkbook');
        $selectedDateFrom = $request->input('date-from');
        $selectedDateTo = $request->input('date-to');

        $checkbook = ($selectedCheckbook == null) ? Checkbook::first() : Checkbook::find($selectedCheckbook);

        if($selectedDateFrom != null && $selectedDateTo != null)
        {
            $checks = $checkbook->checks()
                ->where('created_at', '>=', $selectedDateFrom . ' 00:00:00' )
                ->where('created_at', '<=', $selectedDateTo . ' 23:59:59')
                ->get();
        }
        else
        {
            $checks = $checkbook->checks;
        }

        return view('checks.index')
                ->with('checks', $checks)
                ->with('checkbooks', Checkbook::all())
                ->with('selectedCheckbook', $selectedCheckbook)
                ->with('selected');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('checks.create')
                ->with('checkbooks', Checkbook::all())
                ->with('suppliers', Supplier::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'cantidad' => 'required|numeric',
            'concepto' => 'required|max:190',
            'numero_de_factura' => 'max:50',
            'chequera' => 'required',
            'proveedor' => 'required',
        ]);
        
        $checkbook = Checkbook::find($request->input('chequera'));
        $supplier = Supplier::find($request->input('proveedor'));
        $lastCheck = $checkbook->checks()->orderBy('id', 'desc')->first();

        switch($checkbook->id) {
            case 1: //cuenta ceprep
            $lastCheckNumber = sprintf('%04d', ENV('CUENTA_CEPREP_NUMERO_INICIAL'));
            break;
            case 2: //cuenta investigacion
            $lastCheckNumber = sprintf('%04d', ENV('CUENTA_INVESTIGACION_NUMERO_INICIAL'));
            break;
            default:
            $lastCheckNumber = sprintf('%04d', 1);
            break;
        }

        if($lastCheck != null) {
            $lastCheckNumber = sprintf('%04d', (int)$lastCheck->number + 1);
        }

        $check = new Check();
        $check->number = $lastCheckNumber; 
        $check->amount = $request->input('cantidad');
        $check->description = $request->input('concepto');
        $check->invoice_number = $request->input('numero_de_factura');
        $check->checkbook()->associate($checkbook);
        $check->supplier()->associate($supplier);
        
        $check->save();

        return redirect(route('showCheck', ['check' => $check->id]));
    }

    /**
     * Display the specified resource.
     *
     * @param Check $check
     * @return \Illuminate\Http\Response
     */
    public function show(Check $check)
    {
        setlocale ( LC_TIME, "es_MX");
        return view('checks.show')->with('check', $check);
    }

    /**
     * 
     */
    public function showPrintable(Check $check, Request $request)
    {
        setlocale ( LC_TIME, "es_MX");
        switch($request->input('type'))
        {
            case 'comprobante':
                return view('checks.show_printable_ticket')->with('check', $check);
            case 'poliza':
            default:
                return view('checks.showPrintable')->with('check', $check);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Check $check)
    {
        $check->delete();
        
        return back()->with('success', true);
    }
}
