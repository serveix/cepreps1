<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Validator;

use App\Patient;
use App\Payment;
use App\ServicesCharge;

class PaymentController extends Controller
{
    public function create(Request $request) {
        $this->validate($request, [
            'patient' => 'required|numeric|max:99999999',
            'serviceCharge' => 'required|numeric|max:99999999',
            'cantidad_de_abono' => 'Required|numeric|max:99999999',
            'tipo_de_pago' => 'required|max:190',
            'tarjeta' => 'max:5',
        ]);

        $user_id = Auth::user()->id;

        $tarjeta = $request->input('tarjeta');
        if($request->input('tipo_de_pago') == 'Efectivo'){
            $tarjeta = null;
        }

        $payment = new Payment([
            'user_id' => $user_id,
            'patient_id' => $request->input('patient'),
            'services_charge_id' => $request->input('serviceCharge'),
            'amount' => $request->input('cantidad_de_abono'),
            'payment_method' => $request->input('tipo_de_pago'),
            'last_four' => $tarjeta,
        ]);
            
        $id_sc = $request->input('serviceCharge');

        $a = $request->input('cantidad_de_abono');
        $a = $a + $request->input('total_pay');
        $c = $request->input('total_charge');

        if($a >= $c){
            $sc = ServicesCharge::find($id_sc);
            $sc->billing_status = 'PAID';
            $sc->save();
        }
        
        $payment->save();

        $serviceCharge = ServicesCharge::find($payment->services_charge_id);
        $patient = Patient::find($payment->patient_id);
        return view('receipts/paymentReceipt')->with('serviceCharge', $serviceCharge)->
        with('payment', $payment)->with('patient', $patient);
        
        //return back()->with('status', 'Se agrego el abono de forma satisfactoria.');
    }

    public function receipt(Payment $payment) { 
        $serviceCharge = ServicesCharge::find($payment->services_charge_id);
        $patient = Patient::find($payment->patient_id);
        return view('receipts/paymentReceipt')->with('serviceCharge', $serviceCharge)->
        with('payment', $payment)->with('patient', $patient);
    }
}


?>