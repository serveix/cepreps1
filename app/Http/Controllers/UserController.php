<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\User;

class UserController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile(){
        return view('users.profile');
    }

    public function showList() {
    	return view('users.admin')->with('users', User::all());
    }

    public function userProfile() 
    {
        return view('user.profile');
    }

    public function deleteOne(Request $request){
        $userId = User::find($request->input('userId'));
        $userId->delete();
        return back()->with('success', true);
    }
    
}
