<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServicesCharge;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use App\Service;

class ReportController extends Controller
{
    function index() {
        $services_charges = ServicesCharge::all();
        $dates         = collect([]);
        
        foreach($services_charges as $service){
            
            $full = Carbon::parse($service->created_at)->format('F Y');
            $full = $this->spanishMonth($full);
            
            if(!$dates->contains($full))
                $dates->push($full);
            
        }
        return view('reports.services')->with('dates_avai', $dates);
    }

    private function spanishMonth($date) {
        $date = str_replace ( 'January' , 'Enero' , $date );
        $date = str_replace ( 'February' , 'Febrero' , $date );
        $date = str_replace ( 'March' , 'Marzo' , $date );
        $date = str_replace ( 'April' , 'Abril' , $date );
        $date = str_replace ( 'May' , 'Mayo' , $date );
        $date = str_replace ( 'June' , 'Junio' , $date );
        $date = str_replace ( 'July' , 'Julio' , $date );
        $date = str_replace ( 'August' , 'Agosto' , $date );
        $date = str_replace ( 'September' , 'Septiembre' , $date );
        $date = str_replace ( 'October' , 'Octubre' , $date );
        $date = str_replace ( 'November' , 'Noviembre' , $date );
        $date = str_replace ( 'December' , 'Diciembre' , $date );
        return $date;

    }

    private function englishMonth($date) {
        $date = str_replace ( 'Enero' , 'January' , $date );
        $date = str_replace ( 'Febrero' , 'February' , $date );
        $date = str_replace ( 'Marzo' , 'March' , $date );
        $date = str_replace ( 'Abril' , 'April' , $date );
        $date = str_replace ( 'Mayo' , 'May' , $date );
        $date = str_replace ( 'Junio' , 'June' , $date );
        $date = str_replace ( 'Julio' , 'July' , $date );
        $date = str_replace ( 'Agosto' , 'August' , $date );
        $date = str_replace ( 'Septiembre' , 'September' , $date );
        $date = str_replace ( 'Octubre' , 'October' , $date );
        $date = str_replace ( 'Noviembre' , 'November' , $date );
        $date = str_replace ( 'Diciembre' , 'December' , $date );
        return $date;

    }

    function generate(Request $request) {
        $date = $this->englishMonth($request->input('date'));
        $date = Carbon::createFromFormat('F Y',  $date);
        
        // services charges del mes
        $servicesCharges = ServicesCharge::whereMonth('created_at', '=', $date->format('m'))->get();
        $allServices     = Service::all();

        $monthServices = collect([]);

        // get all services from month
        foreach($servicesCharges as $sc){
            $currentServicesCharge = ServicesCharge::find($sc->id);
            if($currentServicesCharge->approval_status == 'ACTIVE')
                foreach($currentServicesCharge->services as $s) 
                    $monthServices->push($s);
        }

        // //group services by name
        // $monthServices = $monthServices->groupBy('name');
        // $monthServices = $monthServices->toArray();
        
        // separate services by price
        $priceK        = collect([]);
        $priceC        = collect([]);
        $priceProfesor = collect([]);
        $priceSpecial  = collect([]);
        $priceCortesy  = collect([]);


        foreach($monthServices as $service){
            if($service->pivot->price == '0.00')
                $priceCortesy->push($service);
            elseif($service->pivot->price == $service->price_k)
                $priceK->push($service);
            elseif($service->pivot->price == $service->price_c)
                $priceC->push($service);
            elseif($service->pivot->price == $service->price_profesor)
                $priceProfesor->push($service);
            else
                $priceSpecial->push($service);
        }

        $priceK        = $priceK->groupBy('name');
        $priceC        = $priceC->groupBy('name');
        $priceProfesor = $priceProfesor->groupBy('name');
        $priceSpecial  = $priceSpecial->groupBy('name');
        $priceCortesy  = $priceCortesy->groupBy('name');
        Log::debug($priceK);
        return view('reports.servicesreport')->with('servicesCharges', $servicesCharges)
                                             ->with('priceK', $priceK)
                                             ->with('priceC', $priceC)
                                             ->with('priceProfesor', $priceProfesor)
                                             ->with('priceSpecial', $priceSpecial)
                                             ->with('priceCortesy', $priceCortesy);
    }

    public function indexServicesAll() {
        $services_charges = ServicesCharge::all();
        $dates         = collect([]);
        
        foreach($services_charges as $service){
            
            $full = Carbon::parse($service->created_at)->format('F Y');
            $full = $this->spanishMonth($full);
            
            if(!$dates->contains($full))
                $dates->push($full);
            
        }
        return view('reports.servicesAll')->with('dates_avai', $dates);
    }

    public function generateServicesAll(Request $request) {
        $date = $this->englishMonth($request->input('date'));
        $date = Carbon::createFromFormat('F Y',  $date);
        $servicesCharges = ServicesCharge::whereMonth('created_at', '=', $date->format('m'))->get();
        $suma = 0.00;
        foreach($servicesCharges as $sc){
            $suma += $sc->total_amount;
        }
        return view('reports.servicesAllReport')->with('servicesCharges', $servicesCharges)->with('total', $suma);
    }
}
