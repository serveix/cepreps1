<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ServicesCharge;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class ApprovalController extends Controller
{

    /** 
     * ************************************
     * 
     * ESTE CONTROLADOR DEBE LLEVAR MIDDLEWARE SOLO 
     * PARA LOS QUE TIENEN PERMISOS DE APROBAR HOJAS DE COBRO
     * 
     * ************************************
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:ADMIN_APPROVALS');
    }





    public function index()
    {
        return view('approvals.costs')
                ->with('servicesCharges', ServicesCharge::where('approval_status', 'PENDING')->get());
    }

    /** 
     * Envia el HTML segun el modal que se requiera (para ver detalles, aprobar
     * o cancelar hojas de cobro).
     * @param Request 
     */
    public function getApprovalModal(Request $request) {
        $id = $request->input('id');
        $modalAction = $request->input('modalAction');
        $servicesCharge = ServicesCharge::find($id);

        switch($modalAction){
            case 'details':
                return view('includes.modals.approvals.details')->with('servicesCharge', $servicesCharge);
            break;
            case 'approve':
                return view('includes.modals.approvals.approve')->with('servicesCharge', $servicesCharge);
            break;
            case 'cancel':
               return view('includes.modals.approvals.cancel')->with('servicesCharge', $servicesCharge);
            break;
        }
    }

    /**
     * Revisa que la contraseña sea correcta y aprueba la hoja de cobro
     */
    public function approve(Request $request){
        // error handling mas especifico
        $validator = Validator::make($request->all(), [
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator);
        }
        
        if (Hash::check( $request->input('password'), Auth::user()->password )){
            //password correct
            $servicesCharge = ServicesCharge::find( $request->input('servicesChargeId') );
            $servicesCharge->approval_status = 'ACTIVE';
            $servicesCharge->save();

            return back()->with('approveSuccess', true);
        }

        return back()->with('approveSuccess', false);
    }

    /**
     * Revisa que la contraseña sea correcta y cancela la hoja de cobro
     */
    public function cancel(Request $request){
        // error handling mas especifico
        $validator = Validator::make($request->all(), [
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator);
        }
        
        if (Hash::check( $request->input('password'), Auth::user()->password )){
            //password correct
            $servicesCharge = ServicesCharge::find( $request->input('servicesChargeId') );
            $servicesCharge->billing_status  = 'CANCELLED';
            $servicesCharge->approval_status = 'CANCELLED';
            $servicesCharge->save();

            return back()->with('cancelSuccess', true);
        }

        return back()->with('cancelSuccess', false);
    }
}
