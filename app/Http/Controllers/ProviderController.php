<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Provider;
use App\ServicesCharge;
use Illuminate\Support\Facades\Auth;

class ProviderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function createNew() {
        return view('providers.new');
    }

    public function addNew(Request $request) {
        $this->validate($request, [
            'nombre' => 'required|max:191',
            'rfc' => 'required|max:191',
            'domicilio' => 'max:191',
            'telefono' => 'max:191',
        ]);

        $patient = new Provider([
            'name' => $request->input('nombre'),
            'rfc' => $request->input('rfc'),
            'domicilio' => $request->input('domicilio'),
            'telefono' => $request->input('telefono'),
        ]);

        $patient->save();

        return back()->with('success', true);
    }

    public function find() {
        return view('providers.admin')->with( 'providers' , Provider::all() );
    }

    public function viewCharges(Provider $provider) {
        return view('providers.charges')->with('provider', $provider);    
    }

    public function newCharge(Provider $provider, Request $request) {
        //'user_id', 'patient_id', 'discount_request_id', 'total_amount', 
        //'billing_status', 'approval_status', 'patient_paid'
        
        $this->validate($request, [
            'concepto' => 'max:191'
        ]);


        $user = Auth::user();

        $charge = new ServicesCharge([
            'total_amount' => $request->input('cantidad'),
            'billing_status' => 'ACTIVE',
            'approval_status' => 'ACTIVE',
            'patient_paid' => NULL,
            'additional_info' => $request->input('concepto'),
        ]);

        $user->servicesCharges()->save($charge);
        $provider->servicesCharges()->save($charge);

        return redirect()->route('viewProviderReceipt', ['charge' => $charge->id]);
    }

    public function viewReceipt(ServicesCharge $charge) {
        return view('receipts.providerCharge')->with('charge', $charge);
    }

    public function deleteCharge(ServicesCharge $charge) {
        $charge->delete();
        return back()->with('success', true);
    }

    public function deleteProvider(Provider $provider) {
        $provider->delete();
        return back()->with('success', true);
    }
}
