<?php

namespace App\Http\Controllers;

use Validator; // para validar los errores un poquito mas avanzado...
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Collection;

use App\Service;
use App\Patient;
use App\ServicesCharge;

class ServiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    // GET method: shows list of services
    public function showList() {
        return view('services.admin')->with('services', Service::all());
    }


    /**
     * addNew Agrega un nuevo servicio que recibe como parametro. Si algo no sale bien envia un 
     * mensaje de error.
     * @param mixed $request 
     * @return mixed 
     */
    public function addNew(Request $request)
    {

        // error handling mas especifico
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|max:190',
            'precio_K' => 'required|numeric|max:99999999',
            'precio_C' => 'required|numeric|max:99999999',
            'precio_Profesor' => 'required|numeric|max:99999999'
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator, 'newService')->withInput();
        }

        $service = new Service([
            'name' => $request->input('nombre'),
            'price_k' => $request->input('precio_K'),
            'price_c' => $request->input('precio_C'),
            'price_profesor' => $request->input('precio_Profesor'),
        ]);

        $service->save();

        // return back()->with(...)
        return redirect('services/admin')->with('status', 'Se ha creado el nuevo registro de servicio.')->with('services', Service::all());
    }

    /**
     * editService Permite editar el servicio
     * @param mixed $request 
     * @return mixed 
     */
    public function editService(Request $request) {
        
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|max:190',
            'precio_K' => 'required|numeric|max:99999999',
            'precio_C' => 'required|numeric|max:99999999',
            'precio_Profesor' => 'required|numeric|max:99999999'
        ]);

        if($validator->fails()) {
            return back()->withErrors($validator, 'editService')->withInput();
        }

        $service = Service::find( $request->input('id') );

        $service->name           = $request->input('nombre');
        $service->price_k        = $request->input('precio_K');
        $service->price_c        = $request->input('precio_C');
        $service->price_profesor = $request->input('precio_Profesor');
        $service->save();

        return back()->with('editSuccess', true);
    }


    /**
     * deleteService Elimina el servicio que se ingresa como parametro y se devuelve un boleano diciendo
     * si se tuvo exito o no la eliminación del servicio
     * @param mixed $request Servicio a ser eliminado
     * @return mixed Mensaje con boleano dentando si fue o no eliminado el servicio
     */
    public function deleteService(Request $request) {
        $validator = Validator::make($request->all(), [
            'password' => 'required',
        ]);

        if($validator->fails()) {
            return back()->withErrors($validator, 'noPassword')->withInput();
        }

        if (Hash::check($request->input('password'), Auth::user()->password)){
            //password correct
            $service = Service::find( $request->input('id') );
            $service->delete();
            return back()->with('deleteSuccess', true);
        }
        
        return back()->with('deleteSuccess', false);
    }

    /**
     * charge Permite visualizar la vista de cobro de servicios para un paciente
     * @param mixed $id: Id del paciente el cual se realizara un cargo de servicios
     * @return mixed Devuelve una vista para el cobro de servicios con el paciente que se ingreso como
     * parametro
     */
    public function charge($id)
    {
        return view('services/chargeServices')->with('patient', Patient::find($id))->with('services', Service::all());
    }

    /**
     * getAllService Devuelve un arreglo con la colección de servicios que hay
     * @return mixed Devuelve un JSON con un arreglo de todos los servicios que haya en la base de datos
     */
    public function getAllService()
    {
        return json_encode(Service::all());
    }

    /**
     * chargeAuthorization Permite verificar la autorización que se ingresa como parametro
     * @param Request $request: Aquí debe de venir el email y la contraseña que el usuario envío para 
     * autenticar la autorización de un precio especial
     * @return validacion $validation: Devuelve una validación con un true si fue autenticado con exito o
     * false si no optuvo la autenticación y un mensaje explicando el suceso.
     */
    public function chargeAuthorization(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|max:190',
            'contrasena' => 'required|max:190',
        ]);

        if ($validator->fails()) {
            $validation = [
                'status' => false,
                'msg' => 'Verifique que la información este correctamente escrita.',
            ];

            return $validation;
        }
        // Hash::check(param1, param2) compara la contrasena sin hash, con la contrasena hasheada de la db y regresa un bool
        if ($request->input('email') == Auth::user()->email && 
            Hash::check( $request->input('contrasena'), Auth::user()->password )){
            //auth correct
            $service = Service::find( $request->input('id') );
            $service->delete();
            $validation = [
                'status' => true,
                'msg'    => 'Datos correctos. Autorizado.',
            ];
        }
        else
        {
            $validation = [
                'status' => false,
                'msg'    => 'Datos incorrectos. No autorizado.',
            ];
        }

        return $validation;
    }

    /**
     * Crea una hoja de cobro y relaciona los servicios
     * correspondientes con ella, asi como los precios en los que
     * se cobro en el momento, por si llegaran a cambiar en algun momento ya se
     * tiene el registro
     * @param Request
     * @return JSONArray
     */
    public function normalChargeOperation(Request $request) {
        
        $validator = Validator::make($request->all(), [
            'amount_type' => 'required',
            'payment_method' => 'required',
            'credit_card' => 'max:4',
            'patient_id' => 'required',
            'service_id'  => 'required',
            'cantidad_pagada' => 'required',
            'unit_amount' => 'required',
            'payment_type' => 'required',
        ]);

        if ($validator->fails()) {
            $validation = [
                'status' => false,
                'msg' => 'Verique que la información este correctamente escrita.',
            ];
            
            return JSON_Encode($validation);
        }

        $user_id = Auth::user()->id;
        $amount_type = $request->input('amount_type'); // 1 si es pago completo; 2 si es con seguimiento
        $payment_method = $request->input('payment_method'); // puede ser: 'Efectivo', 'Tarjeta' o 'Cheque'
        $credit_card = $payment_method == 'Tarjeta' || $payment_method == 'Cheque' ? $request->input('credit_card') : NULL;
        $patient_id = $request->input('patient_id');
        $service_id   = $request->input('service_id'); // contiene los servicios
        $cantidad_pagada = $request->input('cantidad_pagada');
        $unit_amount  = $request->input('unit_amount'); //contiene el array antes de ser explode(), de precios.
        $date = date('Y-m-d H:i:s');
        $services_ids = explode('%;', $service_id);
        $amounts = explode('%;', $unit_amount);
        $billing_status = $amount_type == 1 ? 'PAID' : 'ACTIVE';
        $approval_status = $request->input('approval_status');
        
        Log::debug($request->input('payment_type'));
        $patient_paid = ($request->input('payment_type') == 3) ? $request->input('patient_paid') : $cantidad_pagada;
        
        // 1. CREAMOS LA HOJA DE COBRO DE SERVICIO FINAL
        $charge = new ServicesCharge([
            'user_id'         => $user_id,
            'patient_id'      => $patient_id,
            'total_amount'    => $cantidad_pagada, //total cobrado
            'billing_status'  => $billing_status,
            'approval_status' => $approval_status,
            'patient_paid'    => $patient_paid
        ]); 
 
        // 2. Una vez creada la hoja de cobro de servicios, se asignan las relaciones de los servicios con esta hoja de cobro
        for($i = 0; $i < count($services_ids); $i++){
            $s = Service::find($services_ids[$i]);
            $s->servicesCharges()->save($charge, ['price' => $amounts[$i], 'payment_method' => $payment_method, 
            'last_four' => $credit_card]);
        } 

        $url = route('serviceChargeReceipt', ['servicesCharge' => $charge->id]);

        if($approval_status == 'ACTIVE')
            $validation = [
                'status' => true,
                'msg'    => 'Cargos agregados correctamente.',
                'createdId' => $charge->id,
                'url' => $url,
            ];
        else // debugging purposes
            $validation = [
                'status'          => 'debug',
                'msg'             => 'Se creo hoja de cobro con approval_status PENDING',
                'createdId' => $charge->id,
                'url' => $url,
            ];
        
        

        return JSON_Encode($validation);
    }


    public function paymentRelationship(Patient $patient){
        $sc = $patient->servicesCharges;
        $servicesCharges = $sc->where('approval_status', '!=', 'CANCELLED')
                              ->where('approval_status', '!=', 'PENDING')
                              ->sortBy('created_at', SORT_REGULAR, true);
        return view('services/paymentRelationship')->with('patient', $patient)->with('servicesCharges', $servicesCharges);
    }

    public function searchReceipts(Patient $patient){
        return view('services/searchReceipts')->with('patient', $patient);
    }

    public function checkCostsApproval(ServicesCharge $servicesCharge)
    {
        return $servicesCharge->approval_status;
    }
    
    public function SCReceipt($serviceCharge){
        $serviceCharge = ServicesCharge::find($serviceCharge);
		if($serviceCharge->approval_status == "PENDING"){
			return redirect(route('allServicesCharges'));
		}
        //$patient = Patient::find($serviceCharge->patient_id);
		$patient = $serviceCharge->patient ? $serviceCharge->patient : $serviceCharge->provider;
        //echo $serviceCharge;
        return view('receipts/serviceChargeReceipt')->with('servicesCharge', $serviceCharge)
        ->with('patient', $patient);
    }

    public function cancelServicesCharge(Request $request){
        $scId = $request->input('scId');

        $sc = ServicesCharge::find($scId);
        $sc->billing_status  = 'CANCELLED';
        $sc->approval_status = 'CANCELLED';
        $sc->save();

        return back()->with('cancelled', true);

    }

    public function showAll() {
        return view('receipts.all')->with('servicesCharges', ServicesCharge::all() );
    }
}
