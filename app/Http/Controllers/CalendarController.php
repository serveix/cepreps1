<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Appointment;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class CalendarController extends Controller
{

    public function getAppointments($calendar) {

        $appointments = Appointment::where('calendar', $calendar)->get();
        
        $returnable = collect([]);

        foreach ($appointments as $appointment) {
            $current = [
                'id'              => $appointment->id,
                'title'           => $appointment->title,
                'start'           => $appointment->start,
                'end'             => $appointment->end,
                'backgroundColor' => $appointment->color,
                'borderColor'     => $appointment->color
            ];
            $returnable->push($current);
        }

        return $returnable;
    }

    /**
     * Creates new event
     */
    public function newAppointment(Request $request)
    {
        // error handling mas especifico
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'start' => 'required',
            'color' => 'required',
            'calendar' => 'required',
        ]);

        if ($validator->fails()) {
            return [
                'success' => false,
                'msg'     => 'Ocurrio un error inesperado. Fallo Validator en CalendarController.php linea 46. ',
            ];
        }

        // En $request->input() la fecha llega como: "2018-03-04T07:00:00", incluyendo los ",
        // por lo que se incluyen en el formato del cual se creara la fecha, como  un caracter literal \"
        $dateTimeFormat = Carbon::createFromFormat('\"Y-m-d\TH:i:s\"', $request->input('start') );
        
        $appointment = new Appointment([
            'title' => $request->input('title'),
            'start' => $dateTimeFormat->toDateTimeString(),
            'color' => $request->input('color'),
            'calendar' => $request->input('calendar'),
        ]);

        $appointment->save();

        return $appointment->id;

    }

    public function updateEnd(Request $request) {
        // error handling mas especifico
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'end' => 'required',
        ]);

        if ($validator->fails()) {
            return [
                'success' => false,
                'msg'     => 'Ocurrio un error inesperado. Fallo Validator en CalendarController.php linea 79. ',
            ];
        }
        $endDate = Carbon::createFromFormat('Y-m-d\TH:i:s', $request->input('end') );
        
        $appointment = Appointment::find( $request->input('id') );
        $appointment->end = $endDate->toDateTimeString();
        $appointment->save();

        return [
            'success' => true,
            'msg'     => 'Fecha de termino del evento actualizada.',
        ];
    }

    public function move(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'start' => 'required',
        ]);

        if ($validator->fails()) {
            return [
                'success' => false,
                'msg'     => 'Ocurrio un error inesperado. Fallo Validator en CalendarController.php linea 104. ',
            ];
        }

        $startDate = Carbon::createFromFormat('Y-m-d\TH:i:s', $request->input('start') );
        if($request->input('end') != null){
            $endDate   = Carbon::createFromFormat('Y-m-d\TH:i:s', $request->input('end') );
            $ending = $endDate->toDateTimeString();
        } else 
            $ending = null;
            
        $appointment        = Appointment::find( $request->input('id') );
        $appointment->start = $startDate->toDateTimeString();
        $appointment->end   = $ending;
        $appointment->save();

        return [
            'success' => true,
            'msg'     => 'Evento movido de fecha/tiempo.',
        ];

    }

    public function getModal(Appointment $appointment){
        return view('includes.modals.appointments.main')->with('appointment', $appointment);
    }

    public function delete(Request $request) {
        $validator = Validator::make($request->all(), [
            'appointmentId' => 'required',
        ]);

        if ($validator->fails()) {
            return [
                'success' => false,
                'msg'     => 'Ocurrio un error inesperado. Fallo Validator en CalendarController.php linea 135. ',
            ];
        }

        $appointment = Appointment::find($request->input('appointmentId'));
        $appointment->delete();

        $returnable = [
            'success' => true,
            'msg'     => 'Se elimino el evento',
        ];
        return back()->with('returnable', $returnable);
    }

    public function updateDesc(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return [
                'success' => false,
                'msg'     => 'Ocurrio un error inesperado. Fallo Validator en CalendarController.php linea 155. ',
            ];
        }

        $appointment = Appointment::find($request->input('id'));
        $appointment->description = $request->input('description');
        $appointment->save();

        return [
            'success' => true,
            'msg'     => 'Se actualizo la descripcion del evento',
        ];

    }
}
