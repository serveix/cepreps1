<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'name', 'price_k', 'price_c', 'price_profesor'
    ];

    function servicesCharges() {
    	return $this->belongsToMany('App\ServicesCharge')->withPivot('price', 'payment_method', 'last_four');
    }
}
