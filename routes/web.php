<?php

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
    if(Auth::guest())
    	return redirect('login');
    else 
    	return redirect('dashboard');
});

Route::get('dashboard', 'HomeController@index');

Route::get('profile', 'UserController@profile');

// -=-=-=-=-=-=-= USERS TAB -=-=-=-=-=-=-=
Route::get('users/new', function() {
	return view('users.new');
});

Route::group(['middleware' => ['permission:ADMIN_USERS']], function () {
    Route::get('users/admin', 'UserController@showList');
	Route::get('users/{case}/{mail}/{pass}', 'UserController@authConfirmation');
	Route::post('users/delete', 'UserController@deleteOne')->name('deleteUser');
});


// -=-=-=-=-=-=-= PATIENTS TAB -=-=-=-=-=-=-=
Route::get('patients/new', 'PatientController@showNew'); 
Route::post('patients/new', 'PatientController@addNew')->name('addNewPatient');

Route::get('patients/edit/{id}', 'PatientController@editPatient')->name('editPatient');
Route::post('patients/edit', 'PatientController@update')->name('updatePatient');

Route::get('patients/admin', 'PatientController@showList');
Route::get('patients/id/{id}', 'PatientController@showOne')->name('patientDetails');

Route::get('patients/find', 'PatientController@findPatient')->name('findPatient');
Route::post('patients/find', 'PatientController@findAndShowPatient')->name('findAndShowPatient');
Route::get('patients/search/{var}/{numresults}', 'PatientController@showPatient'); 

// -=-=-=-=-=-=-= HOJAS DE COBRO TAB -=-=-=-=-=-=-=
Route::get('services/charge/{id}', 'ServiceController@charge')->name('chargeServices');
Route::post('services/charge/auth', 'ServiceController@chargeAuthorization')->name('chargeAuth');

Route::group(['middleware' => ['permission:ADMIN_SERVICES']], function () {
	Route::get('services/admin', 'ServiceController@showList');
	Route::post('services/admin', 'ServiceController@addNew')->name('addNewService');
	Route::post('services/edit', 'ServiceController@editService')->name('editService');
	Route::post('services/delete', 'ServiceController@deleteService')->name('deleteService');
});

Route::get('services/getAll', 'ServiceController@getAllService')->name('getAllServices');

Route::post('services/ChargeNewService', 'ServiceController@normalChargeOperation')->name('ChargeServicesOperation');

// //-=-=-=-=-=-=-= APPOINTMENTS TAB -=-=-=-=-=-=-=-=-=
// Route::get('patients/{id}/appointment', 'AppointmentController@showNewAppointment')->name('newAppointment');
// Route::post('patients/{id}/appointment/new', 'AppointmentController@addNewAppointment')->name('addNewAppointment');
// Route::get('appointments/all', 'AppointmentController@getAppointments')->name('getAppointments');
// Route::get('appointments/{appointment}/modal', 'AppointmentController@getModal');
// Route::post('appointments/delete', 'AppointmentController@deleteOne')->name('deleteAppointment'); 

//-=-=-=-=-=-=-= DOCTORS TAB -=-=-=-=-=-=-=-=-=
Route::get('doctor', function() {
	return view('doctor');
});

//-=-=-=-=-=-=-= Payment TAB -=-=-=-=-=-=-=-=-=
Route::get('payment/create', function() {
	return view('services.payments.create');
}); 

Route::post('payment/create', 'PaymentController@create')->name('createPay');

//-=-=-=-=-=-=-= SCHEDULE TAB -=-=-=-=-=-=-=-=-=
Route::get('payrelationship', function() {
	return view('services.payRelationships');
});

//-=-=-=-=-=-=-= SEARCH SERVICES TAB -=-=-=-=-=-=-=-=-=
Route::get('services/searchreceipts/{patient}', 'ServiceController@searchReceipts')->name('searchreceipts');
Route::get('services/paymentRelationship/{patient}', 'ServiceController@paymentRelationship')->name('paymentRelationship');

//-=-=-=-=-=-=-= SUPPLIERS TAB -=-=-=-=-=-=-=-=-=
Route::get('suppliers', 'SupplierController@index')->middleware('permission:ADMIN_SUPPLIERS');
Route::get('suppliers/new', 'SupplierController@create')->middleware('permission:ADMIN_SUPPLIERS');
Route::post('suppliers/store', 'SupplierController@store')->name('storeSupplier')->middleware('permission:ADMIN_SUPPLIERS');
Route::post('suppliers/{supplier}/destroy', 'SupplierController@destroy')->name('destroySupplier')->middleware('permission:ADMIN_SUPPLIERS');

//-=-=-=-=-=-=-= CHECKS TAB -=-=-=-=-=-=-=-=-=
Route::get('checks', 'CheckController@index')->name('indexChecks')->middleware('permission:ADMIN_CHECKS');
Route::get('checks/new', 'CheckController@create')->middleware('permission:ADMIN_CHECKS');
Route::post('checks/store', 'CheckController@store')->name('storeCheck')->middleware('permission:ADMIN_CHECKS'); 
Route::get('checks/{check}', 'CheckController@show')->name('showCheck')->middleware('permission:ADMIN_CHECKS');
Route::get('printable/checks/{check}', 'CheckController@showPrintable')->name('showPrintableCheck')->middleware('permission:ADMIN_CHECKS');
Route::post('checks/{check}/destroy', 'CheckController@destroy')->name('destroyCheck')->middleware('permission:ADMIN_CHECKS');
//-=-=-=-==-=-=-= APPROVALS =-=-==--=-=-=-=-=-=-=-=
Route::get('approvals/costs', 'ApprovalController@index');
Route::get('approvals/costs/modals', 'ApprovalController@getApprovalModal')->name('getApprovalModal');
Route::post('approvals/costs/approve', 'ApprovalController@approve')->name('approveCosts');
Route::post('approvals/costs/cancel', 'ApprovalController@cancel')->name('cancelCosts');

/* Esta ruta pertenece a approvals pero se encuentra en ServiceController por que ApprovalController 
   tiene un middleware que es solo para usuarios con permisos de aprobar */
Route::get('approvals/costs/check/{servicesCharge}', 'ServiceController@checkCostsApproval')->name('checkCostsApproval');

//-=-=-=-=-=-=-=-= RECEIPTS =-=-=-==-=-=-=-=-=-=-=-=
Route::get('services/receipts', 'ServiceController@showAll')->name('allServicesCharges')->middleware('permission:ADMIN_RECEIPTS');
Route::get('receipts/paymentReceipt/{payment}', 'PaymentController@receipt')->name('paymentReceipt');
Route::get('receipts/serviceChargeReceipt/{servicesCharge}', 'ServiceController@SCReceipt')->name('serviceChargeReceipt');
Route::post('receipts/serviceChargeReceipt/cancel', 'ServiceController@cancelServicesCharge')->name('cancelServicesCharge');

//-=-=-=-=-=-=-= CALEDNAR API -=-=-=-=-=-=-=-=-=
Route::get('calendar/appointments/{calendar}', 'CalendarController@getAppointments');
Route::post('calendar/appointments/new', 'CalendarController@newAppointment');
Route::post('calendar/appointments/update/end', 'CalendarController@updateEnd');
Route::post('calendar/appointments/move', 'CalendarController@move');
Route::get('calendar/appointments/{appointment}/modal', 'CalendarController@getModal');
Route::post('calendar/appointments/delete', 'CalendarController@delete')->name('deleteAppointment');
Route::post('calendar/appointments/update/description', 'CalendarController@updateDesc')->name('updateDescription');

//-=-=-=-=-=-=-= REPORTS -=-=-=-=-=-=-=-=-=
Route::group(['middleware' => ['permission:ADMIN_SERVICES']], function () {
	Route::get('reports/services', 'ReportController@index');
	Route::post('reports/services/generate', 'ReportController@generate')->name('generateReport');
	Route::get('reports/services/all', 'ReportController@indexServicesAll');
	Route::post('reports/services/all/generate', 'ReportController@generateServicesAll')->name('generateServicesAllReport');
});

//-=-=-=-=-=-=-= PROVIDERS -=-=-=-=-=-=-=-=-=
Route::group(['middleware' => ['permission:PROVIDERS']], function () {
	Route::get('providers/new', 'ProviderController@createNew');
	Route::post('providers/new', 'ProviderController@addNew')->name('addNewProvider');
	Route::get('providers/{provider}/charges', 'ProviderController@viewCharges')->name('viewProviderCharges');
	Route::get('providers/charges/{charge}', 'ProviderController@viewReceipt')->name('viewProviderReceipt');
	Route::post('providers/{provider}/charges/generate', 'ProviderController@newCharge')->name('newProviderCharge');
	Route::get('providers/find', 'ProviderController@find');
	Route::post('providers/charges/{charge}/delete', 'ProviderController@deleteCharge')->name('deleteProviderReceipt');
	Route::post('providers/{provider}/delete', 'ProviderController@deleteProvider')->name('deleteProvider');
});