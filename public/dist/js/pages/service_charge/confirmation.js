var serviceChargePromise = new ServiceCharge(); // Desde un principio creamos un ServiceCharge instance, sin info aun.
var confirmationModal    = $('#specialpricefinal');
var loadingApproval      = $('#loading-approval');
var approvedModal        = $('#approved-modal');
var cancelledModal       = $('#cancelled-modal');
/**
 * Revisa si la Hoja de Cobro solicitada con el ID se encuentra pendiente. Sigue corriendo hasta que se 
 * aprueba y despues ejecuta las acciones necesarias (cerrar y abrir los modals necesarios.)
 * @param {id}
 */
function approvalProcess(createdId, url)
{
	checkApproval( createdId ).done(function(result) {
		console.log(createdId);
		console.log(url);
		switch(result){
			case 'PENDING':
			    setTimeout( approvalProcess(createdId, url), 3000 );
			break;
			
			case 'ACTIVE': 
				loadingApproval.modal('toggle');
				approvedModal.modal('toggle');
				console.log(url);
				window.location.href = url;
			break;

			case 'CANCELLED':
			    loadingApproval.modal('toggle');
				cancelledModal.modal('toggle');
			break;
			default:
			    console.log('Ocurrio un error del lado del servidor. Se recibio respuesta no esperada. En confirmation.js linea 28');
		}
			
	});
}
function checkApproval(servicesChargeId) {
	return $.ajax({
		url: '/approvals/costs/check/' + servicesChargeId,
		type: 'get',
	});
}



// Cuando el usuario da clic en confirmar
$('#btnconfirmcharge').click(function() {
	var serviceIdArray = "";
	var totalAmountArray = "";
	var totalAmount = 0;
	var t = $('#tipo-de-pago').val();
	var numtarj = $('#numerosTarjeta').val();
	var x = $('.serviceinput').map(function () {
		var inputid = $(this).val();
		var json_data; // mas adelante, aqui guardo datos del form de hoja de servicios y se manda a php
		var y = $('.serviceobject').map(function () {
			if(inputid == $(this).attr('id')){
				var unitprice = 0;
				var pricetype = "";
				var radiobtn = $('input[name=optradio]:checked', '#pricechoose').val();
				 
				if('pricec' == radiobtn){
					unitprice = parseFloat($(this).attr('pricec'));
					pricetype = 'Precio C';
				}
				else if('pricek' == radiobtn){
					unitprice = parseFloat($(this).attr('pricek'));
					pricetype = 'Precio K';
				}
				else if('pricep' == radiobtn){
					unitprice = parseFloat($(this).attr('priceprofesor'));
					pricetype = 'Precio Profesor';
				}
				else if('pricespecial' == radiobtn){
					unitprice = parseFloat($(this).attr('pricespecial'));
					pricetype = 'Precio Especial';
				}
				else if('pricefree' == radiobtn){
					unitprice = parseFloat($(this).attr('pricefree'));
					pricetype = 'Cortesía';
				}
				serviceIdArray = serviceIdArray + inputid + "%;";
				totalAmountArray = totalAmountArray + unitprice + "%;";
				totalAmount += unitprice;
			}
		});
	});

	console.log('price is ' + price);
	if(t == '1' || t == '3') //Cuando el pago es en una sola exhibición
		totalAmount = price;
	else
	    totalAmount = $('#cantidad-pagada').val();
	

	if (!numtarj)
		numtarj = null;
		
	/* OJO: Uso slice(0,-2) para quitarle los ultimos %; a los array y que al momento de hacer explode en PHP no 
		se cree un elemento dentro del array de mas y vacio que cause errores */
	json_data = {
		user_id: 				$('#userId').val(), //Id del usuario de la sesion actual
		patient_id: 			$('#patientId').val(), //Id del paciente actual
		payment_method: 		$('#metodo-de-pago option:selected').val(), //String con el metodo de pago: Efectivo, Tarjeta, Cheque
		credit_card: 			numtarj, //4 ultimos numeros de la tarjeta de credito. Se enviaran siempre, asi que PHP debera de verificar si se considera o no esta informacion. Cuando no hay nada aqui se envia un null.
		cantidad_pagada: 		totalAmount, //Cantidad total del Cargo de todos los servicios. Si es pago en una sola exhibicion sera igual a la suma del costo de los servicios, si es por abonos sera la cantidad de dinero abonado 
		amount_type: 			$('#tipo-de-pago').val(), //Numero del tipo de pago: "1": si es pago Completo (una sola vez); "2": si es mediante abonos
		service_id: 			serviceIdArray.slice(0,-2), //es una concatenacion de los Id de todos los servicios cobrados, se unen mediante un "%;" PHP debera de separarlos y crear un arreglo con la informacion
		unit_amount: 			totalAmountArray.slice(0,-2), //es una concatenacion de los precios a los cuales fue vendido los servicios cobrados, se unen mediante un "%;" el orden de los precios es exactamente el mismo que el de los Id, por lo mismo es que se pueden relacionar de esa manera.
		approval_status:        null,
		payment_type:           t,
	}; 

	if(json_data.payment_type == '3') // pago con ayuda 
	    json_data.patient_paid = $('#cantidad-pagada-paciente').val();

	serviceChargePromise.setData(json_data);
	
	if(isspecial)
	    confirmationModal.modal('toggle');
	else
	{
		serviceChargePromise.setApprovalStatus('ACTIVE');
		serviceChargePromise.create();
		serviceChargePromise.reloadPage();
	}
});

// Aqui se confirma que se quiere enviar autorizacion para precio especial
$('#confirm-request').click(function() {
	
	serviceChargePromise.setApprovalStatus('PENDING');
	confirmationModal.modal('toggle');
	loadingApproval.modal('toggle');
	serviceChargePromise.create();

});


