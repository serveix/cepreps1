typespecialprice = true;
var isspecial = false;
var price = 0;
var room = 1;
html = $('#serviceminus').html();

/**
 * Administra los inputs dropdowns de servicios
*/
function services_fields() {
	room++;
	var objTo = document.getElementById('services_fields')
	var divtest = document.createElement("div");
	divtest.setAttribute("class", "form-group removeclass" + room);
	var rdiv = 'removeclass' + room;
	var btn = '<div class="input-group-btn"><button class="btn btn-danger delete" type="button" onclick="remove_services_fields(' + room +');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></button>';
	
	divtest.innerHTML = '<div class="form-group " ' + rdiv +'><div class="input-group">' + html + btn + ' </div></div>';

	$('#serviceminus button').attr('onclick', 'remove_services_fields(' + room + ');');
	$('#serviceminus button').attr('n', '' +room );
	objTo.appendChild(divtest);
	$('.select2').select2();
	$('.serviceinput').change(function() {
		serviceChange();
	});
	
}

/**
 * Borra input dropdowns de servicios
 * @param {*} rid 
 */
function remove_services_fields(rid) {
	$('.removeclass' + rid).remove();
}

/**
 * Genera el form para el precio especial
*/
function formspecialprice () {
	var numservices = 0;
	var htmlgenerate = '<div class="col-sm-12 container-fluid">';
	var obj = $('.serviceinput').map(function () {
		var inputid = $(this).val();
		//foreach del XML con la lista de servicios
		var services = $('.serviceobject').map(function () {  
			if(inputid == $(this).attr('id')){
				htmlgenerate += '<div class="form-group"><label for "service' + 
				$(this).attr('id') +'">' + $(this).attr('name') +': </label><input ' +
				'type="number" value="' + $(this).attr('pricespecial') +'" ' +
				'placeholder="1,000.00" required="" class="form-control specialpriceinput" id="especial_' + 
				numservices +'" name="' + $(this).attr("name") + '" dbid="' + $(this).attr('id') + '"/></div>';
				numservices ++;
			}
		});
	});
	htmlgenerate += '</div>';
	$("#modalspecialpriceform").html(htmlgenerate);
	isspecial = true;
}


var charge = [];

/** 
 * Se ejecuta esta funcion cada vez que cambian los servicios en el DOM
*/
function serviceChange() {
	charge = [];
	price = 0;
	$('#pricelist').empty();
	$('#finalprice').empty();
		//foreach de los servicios que eligio el usuario
	var obj = $('.serviceinput').map(function () {
		var inputid = $(this).val();
		//foreach del XML con la lista de servicios
		var services = $('.serviceobject').map(function () {  
			if(inputid == $(this).attr('id')){
				var unitprice = 0;
				var pricetype = "";
				var radiobtn = $('input[name=optradio]:checked', '#pricechoose').val();
				if('pricec' == radiobtn){
					unitprice = parseFloat($(this).attr('pricec'));
					pricetype = 'Precio C';
				}
				else if('pricek' == radiobtn){
					unitprice = parseFloat($(this).attr('pricek'));
					pricetype = 'Precio K';
				}
				else if('pricep' == radiobtn){
					unitprice = parseFloat($(this).attr('priceprofesor'));
					pricetype = 'Precio Profesor';
				}
				else if('pricespecial' == radiobtn){
					unitprice = parseFloat($(this).attr('pricespecial'));
					pricetype = 'Precio Especial';
				}
				else if('pricefree' == radiobtn){
					unitprice = parseFloat($(this).attr('pricefree'));
					pricetype = 'Cortesía';
				}
				price += unitprice;
				var htmlinner = '<div class="row"><div class="col-sm-9">' + $(this).attr('name') + '</div> <div class="text-right col-sm-3"><b> $ ' + (unitprice).formatMoney(2) + '</b></div></div>';
				$('#pricelist').append(htmlinner);
			}
		});
	});
	var totalprice = '$ ' + (price).formatMoney(2);
	$('#finalprice').append(totalprice);
	
}

// Cada vez que cambia de precio especial a normal, viceversa o se agregan servicios, esta se ejecuta
$('.serviceinput, .radiopricebtn').change(function() {
	var radiobtn = $('input[name=optradio]:checked').val();
	
	if(radiobtn == 'pricek' || radiobtn == 'pricec' || radiobtn == 'pricep')
		isspecial = false;
	else 
		isspecial = true;
	

	serviceChange();
});

// Inicializa los campos tipo Select2 de AdminLTE
$('.select2').select2();

// Le da formato a los campos de dinero
Number.prototype.formatMoney = function(c, d, t){
	var n = this, 
	c = isNaN(c = Math.abs(c)) ? 2 : c, 
	d = d == undefined ? "." : d, 
	t = t == undefined ? "," : t, 
	s = n < 0 ? "-" : "", 
	i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
	j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

// Se selecciona precio especial
$('#modalspecialpricebtn').click(function () {  
	$('#modalfreepricebtn').removeClass('btn-primary');
	$('#modalfreepricebtn').addClass('btn-default');
	$('#modalspecialpricebtn').removeClass('btn-default');
	$('#modalspecialpricebtn').addClass('btn-primary');
	$("#specialpricemodaltitle").text('Precio Especial');
	$('.modalpricebody').addClass('hidden');
	$('#modalbodyspecialprice').removeClass('hidden');
	typespecialprice = true;
});

// Se selecciona precio cortesia (gratis)
$('#modalfreepricebtn').click(function () {  
	$('#modalspecialpricebtn').removeClass('btn-primary');
	$('#modalspecialpricebtn').addClass('btn-default');
	$('#modalfreepricebtn').removeClass('btn-default');
	$('#modalfreepricebtn').addClass('btn-primary');
	$("#specialpricemodaltitle").text('Cortesía');
	$('.modalpricebody').addClass('hidden');
	$('#modalbodyfreeprice').removeClass('hidden');
	typespecialprice = false;
});

// Se genera el form cuando se elije precio especial
$('#specialprice').click(function () {  
	formspecialprice ();
	$('#label-pricespecial').removeClass('hidden');
	$('#label-pricefree').addClass('hidden');
	$('#radiopricespecial').prop("checked", true);
});

// Clic en guardar del modal que sale para precio especial o cortesia
$('#btnsavespecialprice').click(function () {
	if (typespecialprice) {
		var success = true;  
		$('.autherrors').addClass('hidden');
		$('#autherrorstext').empty();
		var inp = $('.specialpriceinput').map(function () {  
			if(($(this).val() == null) || ($(this).val() == '')){
				$('.autherrors').removeClass('hidden');
				$('#autherrorstext').append('El valor en "' + $(this).attr('name') + '" es incorrecto.<br>');
				success = false;
			}
			else{
				var val = $(this).val();
				var id = $(this).attr('dbid');
				var ojserv = $('.serviceobject').map(function () {  
					if($(this).attr('id') == id){
						$(this).attr('pricespecial', val);
					}
				});
			}
		});
		if(success){ // si se aplicara precio especial
			$('#specialpricemodal').modal('toggle');
			$('#label-pricespecial').removeClass('hidden');
			$('#label-pricefree').addClass('hidden');
			$('#radiopricespecial').prop("checked", true);
			
			serviceChange();
			isspecial = true;
		}
	} else { // si se aplicara cortesia (precio gratis totalmente)
		$('#specialpricemodal').modal('toggle');
		$('#label-pricefree').removeClass('hidden');
		$('#label-pricespecial').addClass('hidden');
		$("#radiopricefree").prop("checked", true);
		serviceChange();
		isspecial = true;
	}
});

// Selecciona metodo de pago tarjeta o cheque y mostramos campo de ultimos 4 digitos
$('#metodo-de-pago').change(function(){
	var value = $(this).val();
	if(value ==  'Tarjeta' || value == 'Cheque')
	    $('#last-four').removeClass('hidden');
	else
	    $('#last-four').addClass('hidden');
});

// Selecciona metodo de pago tarjeta o cheque y mostramos campo de ultimos 4 digitos
$('#tipo-de-pago').change(function(){
	var value = $(this).val();
	if(value == 2){
		$('#amount-payed').removeClass('hidden');
		$('#amount-payed-patient').addClass('hidden');
	}
	else if(value == 3){
		$('#amount-payed').addClass('hidden');
		$('#amount-payed-patient').removeClass('hidden');
	}
	else{
		$('#amount-payed').addClass('hidden');
		$('#amount-payed-patient').addClass('hidden');
	}
});