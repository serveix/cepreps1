function ServiceCharge()
{
    this.data;
    this.status;
    this.createdId = 0;

    /**
     * Inserta la informacion que sera usada para crear
     * la hoja de cobro de servicio
     * @param {String} data 
     */
    this.setData = function(data) {
        this.data = data;
    };

    /**
     * Inserta el estatus de aprobacion de la hoja. Puede ser
     * ACTIVE o PENDING
     * @param {String} status 
     */
    this.setApprovalStatus = function(status) {
        this.status = status;
        this.data.approval_status = this.status;
    } 

    /**
     * Se crea la hoja de cobro y si se crea con approval_status = PENDING (pendiente de aprobar) por que tiene un
     * precio especial, entonces tambien regresa el ID de la hoja creada.
    */
    this.create = function(){
        var url = $('#directionURL').attr('url');
        
        $.post(url, this.data, function (result) {  
                var t = JSON.parse(result); 
                
                if(t.status == true)
                {
                    $('#globalsuccessmsg').removeClass('hidden');
                    $('#globalerrormsg').addClass('hidden');
                    $('#globalsuccessmsg').text(t.msg);
                    redireccionar(t.url);
                } else if(t.status == false) {
                    $('#globalsuccessmsg').addClass('hidden');
                    $('#globalerrormsg').removeClass('hidden');
                    $('#globalerrormsg').text(t.msg);
                } else { //t.status == 'debug' (SE CREO EN PENDING)
                    console.log(t.url);
                    approvalProcess(t.createdId, t.url);
                }
        }); 
    }

    this.reloadPage = function() {
        console.log(123);
    }

    function redireccionar(url){
        window.location.href = url;
    }

    $('#url-button').click(function() {

    });

}