var infoModalContainer = $('#appointment-info-modal-container');

function getModalInfo( appointmentId ){
  return $.ajax('/appointments/'+appointmentId+'/modal', 'get');
}

function postAppointment(url, data){
  return $.ajax({
    url: url, 
    type: 'post', 
    data: data,
  });
}

function getAppointment(url){
  return $.ajax({
    url: url, 
    type: 'get',
  });
}

$('a[style*="backgroundColor: rgb(216, 27, 96)"]').hide();

//dom ready
$(document).ready(function() {
    
    /* initialize the external events
     -----------------------------------------------------------------*/
     function init_events(ele) {
      ele.each(function () {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
          title: $.trim($(this).text()) // use the element's text as the event title
        }

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject)

        // make the event draggable using jQuery UI
        $(this).draggable({
          zIndex        : 1070,
          revert        : true, // will cause the event to go back to its
          revertDuration: 0  //  original position after the drag
        })

      })
    }

    init_events($('#external-events div.external-event'))

    //esto hace que no falle con bootstrap tabs
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      console.log("renderizado cal");
      $('#calendar-pruebas').fullCalendar( 'render' )
      $('#calendar-pruebas').fullCalendar( 'rerenderEvents' )
      $('#calendar-consultas').fullCalendar( 'render' )
      
  });
    

    /* ************************************************************
     * * CALENDARIO CONSULTAS 
     * * **********************************************************/

    
    $('#calendar-consultas').fullCalendar({
        header    : {
          left  : 'prev,next today',
          center: 'title',
          right : 'agendaWeek,month,agendaDay'
        },
        defaultView: 'agendaWeek',
        monthNames: [
          'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 
          'Septiembre', 'Octubre', 'Noviembre', 'Diciembre',
        ],
        dayNames: [
          'domingo', 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado',
        ],
        dayNamesShort: [
          'Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab',
        ],
        buttonText: {
          today:    'Hoy',
          month:    'Mes',
          week:     'Semana',
          day:      'Dia',
          list:     'Lista'
        },
        events: '/calendar/appointments/1', //api para todos los eventos,
        editable: true,
        defaultTimedEventDuration: '01:00:00',
        droppable: true,
        drop      : function (date, allDay) { // this function is called when something is dropped

          var willBeId;
          var titleSourceElem = $(this).find('input');
          var finalTitle;
          console.log(titleSourceElem)
          if(titleSourceElem.val() == ''){
              finalTitle =  $(this).text()
              var foo = finalTitle.split(':')
              finalTitle = foo[0]
          } 
          else
              finalTitle = titleSourceElem.val()
              

          // date = "2018-03-04T06:30:00"
          // JSON con info necesaria para la bd nada mas
          var dbObject = {
            title: finalTitle,
            start: JSON.stringify(date),
            color: rgb2hex( $(this).css('background-color') ),
            calendar: 1,
          }

          // retrieve the dropped element's stored Event Object
          var originalEventObject = $(this).data('eventObject')
  
          // we need to copy it, so that multiple events don't have a reference to the same object
          var copiedEventObject = $.extend({}, originalEventObject)
  
          // assign it the date that was reported
          copiedEventObject.title           = finalTitle
          copiedEventObject.start           = date
          copiedEventObject.allDay          = allDay
          copiedEventObject.backgroundColor = $(this).css('background-color')
          copiedEventObject.borderColor     = $(this).css('border-color')
          
          
          postAppointment('/calendar/appointments/new', dbObject).done(function(result) {
              copiedEventObject.id = result
              // render the event on the calendar
              // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
              $('#calendar-consultas').fullCalendar('renderEvent', copiedEventObject, true)
              titleSourceElem.val('')
          })
        },
        eventResize: function(event, delta, revertFunc) {
          var dbObject = {
            id: event.id,
            end: event.end.format(),
          }
          postAppointment('/calendar/appointments/update/end', dbObject).done(function(result) {
              console.log(result);
          })
      
        },
        eventDrop: function(event, delta, revertFunc) { //moving event to another day/time
          var endDate;
          if(event.end == null) 
            endDate = null;
          else 
            endDate = event.end.format();

          var dbObject = {
            id: event.id,
            start: event.start.format(),
            end: endDate,
          }
          
          postAppointment('/calendar/appointments/move', dbObject).done(function(result) {
            console.log(result);
          })
      
        },
        eventClick: function(calEvent, jsEvent, view) {
          // alert('Event: ' + calEvent.title);
          // alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
          // alert('View: ' + view.name);
          // // change the border color just for fun
          // $(this).css('border-color', 'red');
          getAppointment('/calendar/appointments/' + calEvent.id + '/modal').done(function(result) {
              
              $('#appointment-info-modal-container').html(result);
              $('#appointment-info-modal').modal('toggle');
              
              $('#descForm').submit(function(e) {
                e.preventDefault();
              
                postAppointment('/calendar/appointments/update/description', {
                  id: calEvent.id,
                  description: $('#desc').val()
                }).done(function(result) {
                  console.log(result);
                  $('#success-label').removeClass('hidden');
                  $('#success-label').html(result.msg);
                  
                })
              })

          })
      
        }
    })



    /* ************************************************************
     * * CALENDARIO PRUEBAS
     * * **********************************************************/

    $('#calendar-pruebas').fullCalendar({
      header    : {
        left  : 'prev,next today',
        center: 'title',
        right : 'agendaWeek,month,agendaDay'
      },
      defaultView: 'agendaWeek',
      monthNames: [
        'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 
        'Septiembre', 'Octubre', 'Noviembre', 'Diciembre',
      ],
      dayNames: [
        'domingo', 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado',
      ],
      dayNamesShort: [
        'Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab',
      ],
      buttonText: {
        today:    'Hoy',
        month:    'Mes',
        week:     'Semana',
        day:      'Dia',
        list:     'Lista'
      },
      events: '/calendar/appointments/2', //api para todos los eventos,
      editable: true,
      defaultTimedEventDuration: '01:00:00',
      droppable: true,
      drop      : function (date, allDay) { // this function is called when something is dropped

        var willBeId;
        var titleSourceElem = $(this).find('input');
        var finalTitle;
        console.log(titleSourceElem)
        if(titleSourceElem.val() == ''){
            finalTitle =  $(this).text()
            var foo = finalTitle.split(':')
            finalTitle = foo[0]
        } 
        else
            finalTitle = titleSourceElem.val()
            

        // date = "2018-03-04T06:30:00"
        // JSON con info necesaria para la bd nada mas
        var dbObject = {
          title: finalTitle,
          start: JSON.stringify(date),
          color: rgb2hex( $(this).css('background-color') ),
          calendar: 2,
        }

        // retrieve the dropped element's stored Event Object
        var originalEventObject = $(this).data('eventObject')

        // we need to copy it, so that multiple events don't have a reference to the same object
        var copiedEventObject = $.extend({}, originalEventObject)

        // assign it the date that was reported
        copiedEventObject.title           = finalTitle
        copiedEventObject.start           = date
        copiedEventObject.allDay          = allDay
        copiedEventObject.backgroundColor = $(this).css('background-color')
        copiedEventObject.borderColor     = $(this).css('border-color')
        
        
        postAppointment('/calendar/appointments/new', dbObject).done(function(result) {
            copiedEventObject.id = result
            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#calendar-pruebas').fullCalendar('renderEvent', copiedEventObject, true)
            titleSourceElem.val('')
        })
      },
      eventResize: function(event, delta, revertFunc) {
        var dbObject = {
          id: event.id,
          end: event.end.format(),
        }
        postAppointment('/calendar/appointments/update/end', dbObject).done(function(result) {
            console.log(result);
        })
    
      },
      eventDrop: function(event, delta, revertFunc) { //moving event to another day/time
        var endDate;
        if(event.end == null) 
          endDate = null;
        else 
          endDate = event.end.format();

        var dbObject = {
          id: event.id,
          start: event.start.format(),
          end: endDate,
        }
        
        postAppointment('/calendar/appointments/move', dbObject).done(function(result) {
          console.log(result);
        })
    
      },
      eventClick: function(calEvent, jsEvent, view) {
        // alert('Event: ' + calEvent.title);
        // alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
        // alert('View: ' + view.name);
        // // change the border color just for fun
        // $(this).css('border-color', 'red');
        getAppointment('/calendar/appointments/' + calEvent.id + '/modal').done(function(result) {
            
            $('#appointment-info-modal-container').html(result);
            $('#appointment-info-modal').modal('toggle');
            
            $('#descForm').submit(function(e) {
              e.preventDefault();
            
              postAppointment('/calendar/appointments/update/description', {
                id: calEvent.id,
                description: $('#desc').val()
              }).done(function(result) {
                console.log(result);
                $('#success-label').removeClass('hidden');
                $('#success-label').html(result.msg);
                
              })
            })

        })
    
      }
  })

})