<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistema CEPREP</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
  
  <!-- Ionicons -->
  <link rel="stylesheet" href="/bower_components/Ionicons/css/ionicons.min.css">
  <!-- select 2 -->
   <link rel="stylesheet" href="/bower_components/select2/dist/css/select2.min.css">
  <!--Theme-->
   <link rel="stylesheet" href="/dist/css/AdminLTE.css">
   <!--Datatables-->
   <link rel="stylesheet" type="text/css" href="{{asset('dist/datatables/datatables.min.css')}}"/>
   <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
   <link rel="icon" href="/favicon.ico" type="image/x-icon">

   <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="/dist/css/skins/skin-blue.min.css">



  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <!-- Font Awesome -->
  <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">

  <link rel="stylesheet" type="text/css" href="/dist/css/styles.css">

  @yield('styles')
</head>

<body onload="startTime()" class="hold-transition skin-blue @if(!Auth::guest()) sidebar-mini @else sidebar-collapse @endif">

<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>C</b>E</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>CEPREP</b> Empleados</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">

      @if(!Auth::guest())
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <i class="fas fa-bars"></i>
        <span class="sr-only">Toggle navigation</span>
      </a>
      @endif

      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          @if (!Auth::guest())
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs">{{ Auth::user()->name }}</span>
              <input id="userId" class="hidden" value="{{ Auth::user()->id }}">
            </a>

            
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  {{ Auth::user()->name }} - Empleado
                  <small>Miembro desde {{ Auth::user()->created_at }}</small>
                </p>
              </li>
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="profile" class="btn btn-default btn-flat">Perfil</a>
                </div>
                <div class="pull-right">
                  <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">Cerrar sesi&oacute;n</a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                  </form>
                </div>
              </li>
            </ul>
           </li>
           @endif

           <li><a href="#"><span id="dateDisplay"></span></a></li>
           <li><a href="#"><span id="timeDisplay"></span></a></li>
      
          
        </ul>
      </div>
    </nav>
  </header>

  @if(!Auth::guest())
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      

      <!-- Sidebar Menu -->
      {{-- TODO: Change all links to laravel route() --}}

      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU</li>
        
        <li class="{{ Request::path() ==  'dashboard' ? 'active' : ''}}">
          <a href="/"><i class="fas fa-calendar-alt"></i> <span>Calendario</span></a>
        </li>

        @if(Auth::user()->hasPermission('ADMIN_USERS') == 'yes')
        <li class="treeview {{ Request::path() == 'register' || Request::path() == 'users/admin' ? 'active' : '' }}">
          <a href="#">
            <i class="fa fa-briefcase" aria-hidden="true"></i>
            <span>Empleados </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ Request::path() ==  'register' ? 'active' : ''}}"><a href="/register">Agregar nuevo</a></li>
            <li class="{{ Request::path() ==  'users/admin' ? 'active' : ''}}"><a href="/users/admin">Administraci&oacute;n de usuarios</a></li>

          </ul>
        </li>
        @endif

        <li class="treeview {{ Request::path() ==  'patients/new' || Request::path() == 'patients/find' ? 'active' : ''}}">
          <a href="#"><i class="fa fa-users"></i> <span>Pacientes</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ Request::path() == 'patients/new' ? 'active' : '' }}"><a href="/patients/new">Registrar Paciente</a></li>
            {{--  <li class="{{ Request::path() == 'patients/admin' ? 'active' : '' }}"><a href="/patients/admin">Administraci&oacute;n de pacientes</a></li>  --}}
            <li class="{{ Request::path() == 'patients/find' ? 'active' : '' }}"><a href="/patients/find">Buscar Paciente</a></li>
            
          </ul>
        </li>
		
		@if(Auth::user()->hasPermission('PROVIDERS') == 'yes')
        <li class="treeview {{ Request::path() ==  'providers/new' || Request::path() == 'providers/find' ? 'active' : ''}}">
          <a href="#"><i class="fas fa-hand-holding"></i> <span>Proveedores</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ Request::path() == 'providers/new' ? 'active' : '' }}"><a href="/providers/new">Registrar Proveedor</a></li>
            <li class="{{ Request::path() == 'providers/find' ? 'active' : '' }}"><a href="/providers/find">Administrar Proveedores</a></li>
          </ul>
        </li>
        @endif

        @if(Auth::user()->hasPermission('ADMIN_SERVICES') == 'yes')
        <li class="{{ Request::path() ==  'services/admin' ? 'active' : ''}}">
            <a href="/services/admin"><i class="far fa-hospital"></i> <span>Servicios del Hospital</span></a>
        </li>
        @endif

        

        @if(Auth::user()->hasPermission('ADMIN_RECEIPTS') == 'yes')
        <li class="{{ Request::path() ==  'services/receipts' ? 'active' : ''}}">
            <a href="{{ route('allServicesCharges') }}"><i class="fas fa-list-ul"></i> <span>Recibos generados</span></a>
        </li>
        @endif
		

        @if(Auth::user()->hasPermission('ADMIN_APPROVALS') == 'yes')
        <li class="{{ Request::path() ==  'approvals/costs' ? 'active' : ''}}">
          <a href="/approvals/costs"><i class="fas fa-thumbs-up"></i> <span>Aprobaciones Costos Especiales</span></a>
        </li>
        @endif
		
		@if(Auth::user()->hasPermission('REPORTS') == 'yes')
        <li class="treeview {{ Request::path() == 'reports/services' || Request::path() == 'reports/services/all' ? 'active' : '' }}">
          <a href="#"><i class="fa fa-file-alt"></i> <span>Reportes</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ Request::path() ==  'reports/services' ? 'active' : ''}}">
              <a href="/reports/services">Informaci&oacute;n de Servicios</a>
            </li>
            <li class="{{ Request::path() ==  'reports/services/all' ? 'active' : ''}}">
              <a href="/reports/services/all">Todos los Servicios</a>
            </li>
          </ul>
        </li>
        @endif
		
		@if(Auth::user()->hasPermission('ADMIN_SUPPLIERS') == 'yes')
        <li class="treeview {{ Request::path() == 'suppliers' || Request::path() == 'suppliers/new' ? 'active' : '' }}">
          <a href="#">
            <i class="fa fa-truck"></i>
            <span>Acreedores </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ Request::path() ==  'suppliers' ? 'active' : ''}}"><a href="/suppliers">Administraci&oacute;n de acreedores</a></li>
            <li class="{{ Request::path() ==  'suppliers/new' ? 'active' : ''}}"><a href="/suppliers/new">Agregar nuevo</a></li>

          </ul>
        </li>
		@endif

		@if(Auth::user()->hasPermission('ADMIN_CHECKS') == 'yes')
        <li class="treeview {{ Request::path() == 'checks' || Request::path() == 'checks/new' ? 'active' : '' }}">
          <a href="#">
            <i class="fa fa-money-check-alt"></i>
            <span>Cheques </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ Request::path() ==  'checks' ? 'active' : ''}}"><a href="/checks">Administraci&oacute;n de cheques</a></li>
            <li class="{{ Request::path() ==  'checks/new' ? 'active' : ''}}"><a href="/checks/new">Generar nuevo</a></li>

          </ul>
        </li>
		@endif

      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  @endif

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        @yield('page-header')
        <small>@yield('page-header-small')</small>
      </h1>
      <ol class="breadcrumb">
        @yield('breadcrumb')
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

    @yield('content')

    </section>
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="ppull-right hidden-xs">
      Versi&oacute;n 1.1.0
    </div>
    <!-- Default to the left -->
    Sistema <strong><a href="#">CEPREP</a></strong> Empleados
  </footer>

</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="/dist/js/adminlte.min.js"></script>
<script src="/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- SlimScroll -->
<script src="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/bower_components/fastclick/lib/fastclick.js"></script>
<!-- webshim-->
<script src="/bower_components/webshim/js-webshim/minified/polyfiller.js"></script>
<!-- polyfill-->
<script src="/bower_components/polyfill.js"></script>
<!--Datatable-->
<!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
<script type="text/javascript" src="{{ asset('dist/datatables/datatables.min.js') }}"></script>

<!--AJAX CSRF PROTECTION -->
<script>
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
</script>


<!-- Clock script-->
<script>
window.onload = date_time();

function date_time()
{
  date = new Date;
  
  year = date.getFullYear();
  
  month = date.getMonth();
  months = new Array('Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
  
  d = date.getDate();
  day = date.getDay();
  days = new Array('Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab');
  
  h = date.getHours();

  if(h<10)
      h = "0"+h;
  
  m = date.getMinutes();

  if(m<10)
      m = "0"+m;
  
  s = date.getSeconds();

  if(s<10)
      s = "0"+s;
  
  resultDate = days[day]+' '+d+' '+months[month]+' '+year;
  resultHour = h+':'+m+':'+s;

  document.getElementById('dateDisplay').innerHTML = resultDate;
  document.getElementById('timeDisplay').innerHTML = resultHour;

  setTimeout('date_time();','1000');
  return true;
}
</script>

@yield('scripts')

</body>
</html>