@extends('layouts.default') @section('content') @section('page-header', 'Recibo de Pago') @section('page-header-small') Recibo
de Pago
<div class="container">
    <div class="row">
        <div class="col-xs-1 col-xs-offset-10">
            <a id="toPrint" target="_blank" class="btn btn-primary">Imprimir
                <i class="fas fa-print"></i>
            </a>
        </div>
    </div>
</div>
@endsection

<?php 
$cargo = 0;
$abono = 0;
    foreach ($serviceCharge->services as $s) {
        $cargo = $cargo + $s->pivot->price;
    }
    foreach ($serviceCharge->payments as $pay) {
        $abono = $abono + $pay->amount;
    }
    $total = $cargo - ($serviceCharge->total_amount + $abono);
?>

<style>
    .row div{
        padding-bottom: 8px;
    }
</style>

<div class="content container-fluid">
    <div class="row">
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-3">
                    <img src="/dist/img/logoUANL.jpg" width="100%">
                </div>
                <div class="col-xs-9">
                    <strong>UNIVERSIDAD AUTONOMA DE NUEVO LEON</strong>
                    <br> HOSPITAL UNIVERSITARIO "Dr. Jose Eleuterio Gonzalez"
                    <br> C.E.P.R.E.P.
                    <br> FICHA DE IDENTIFICACI&Oacute;N
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <img src="/dist/img/logoCEPREP.jpg" height="100px" align="right">
        </div>
    </div>
    <div class="row top-buffer justify-content-right">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">PAGO POR ABONO #{{ $payment->id }}</h3>
                </div>
                <div class="box-body">

                    <table class="table table-bordered">
                        <thead>
                            <th>IDENTIFICACIÓN DEL PACIENTE</th>
                        </thead>
                        <tbody>
                            <td>
                                <div class="row">
                                    <div class="col-xs-8">
                                        <strong>Nombre del paciente:</strong>
                                        <br> {{ $patient->name }}
                                    </div>
                                    <div class="col-xs-4">
                                        <strong>Número de Paciente: <br> {{ $patient->id}}</strong>
                                    </div>
                                </div>
                            </td>
                        </tbody>
                    </table>
                    <br>
                    <table class="table table-bordered">
                        <thead>
                            <th>PAGO POR ABONO</th>
                        </thead>
                        <tbody>
                            <td>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <strong>Total de pago:</strong>
                                        <br>$ {{ $payment->amount }}
                                    </div>
                                    <div class="col-xs-4">
                                        <strong>Método de pago:</strong>
                                        <br> {{ $payment->payment_method }}
                                    </div>
                                    <div class="col-xs-4">
                                        <strong>Restante:</strong>
                                        <br>$ {{ $total }}
                                    </div>
                                    @if($payment->last_four != null)
                                    <div class="col-xs-4">
                                        <strong>Últimos 4 números de tarjeta:</strong>
                                        <br> {{ $payment->last_four }}
                                    </div>
                                    @endif
                                    <div class="col-xs-4">
                                        <strong>Fecha:</strong>
                                        <br> {{ $payment->created_at }}
                                    </div>
                                </div>
                            </td>
                        </tbody>
                    </table>
                    <br>
                    <table class="table table-bordered">
                        <thead>
                            <th>DATOS DEL CARGO DE SERVICIOS</th>
                        </thead>
                        <tbody>
                            <td>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <strong>Número de Cargo</strong>
                                        <br>{{ $serviceCharge->id }}
                                    </div>
                                    <div class="col-xs-3">
                                        <strong>Total del cargo:</strong>
                                        <br> $ {{ $cargo }}
                                    </div>
                                    <div class="col-xs-3">
                                        <strong>Estado:</strong>
                                        <br>{{ $serviceCharge->billing_status }}
                                    </div>
                                    <div class="col-xs-3">
                                        <strong>Fecha:</strong>
                                        <br>{{ $serviceCharge->created_at }}
                                    </div>
                                </div>
                            </td>
                        </tbody>
                    </table>
                    <br>
                    <table class="table table-bordered">
                        <thead>
                            <th>SERVICIOS A PAGAR</th>
                        </thead>
                        <tbody>
                            <td>
                                <div class="row" style="padding:8px">
                                    <table class="table table-condensed">
                                        <thead>
                                            <th>SERVICIO</th>
                                            <th>PRECIO</th>
                                        </thead>
                                        <tbody>
                                            <!--Servicios-->
                                            @foreach ($serviceCharge->services as $s)
                                            <tr>
                                                <td>{{ $s->name }}</td>
                                                <td>$ {{ $s->pivot->price }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection @section('scripts')
<script type="text/javascript">
    $('#toPrint').click(function () {
        window.print();
    });
</script> @endsection