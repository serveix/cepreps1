@extends('layouts.default')
@section('page-header', 'Recibos')
@section('page-header-small', 'Recibos y Cuentas por Cobrar Totales')
@section('content')
<div class="content">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @if(session('cancelled'))
            <div class="alert alert-success">
                La hoja de cobro fue cancelada.
            </div>
            @endif
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Todos los servicios creados</h3>
                </div>
                <div class="box-body">
                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Folio</th>
                                <th>Fecha</th>
                                <th>Cantidad</th>
                                <th>Tipo</th>
                                <th>Estado de Aprobaci&oacute;n</th>
                                <th>Paciente</th>
                                <th>Acci&oacute;nes</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($servicesCharges as $sc)
                            <tr>
                                <td>C - {{ $sc->id }}</td>
                                <td>{{ $sc->created_at }}</td>
                                <td>$ {{ $sc->total_amount }}</td>
                                <td>Cargo de Servicio</td>
                                <td>{{ $sc->approval_status }}</td>
                                <td>{{ $sc->patient->name OR $sc->provider->name }}</td>
                                <td class="text-center">
                                    <form action="{{ route('cancelServicesCharge') }}" method="POST">
                                        
										@if($sc->approval_status != "PENDING")
                                        <a href="{{ route('serviceChargeReceipt', ['servicesCharge' => $sc->id]) }}" class="btn btn-link">Ver</a> 
                                        @endif 
										
                                        @if(Auth::user()->hasPermission('ADMIN_CANCEL_RECEIPTS') == 'yes')
                                        {{ csrf_field() }}
                                        <input type="hidden" name="scId" value="{{ $sc->id }}">
                                        <button type="submit" class="btn btn-link">Cancelar</button>
                                        @endif
                                        
                                    </form>
                                </td>
                            </tr>
                                @foreach($sc->payments as $pay)
                                <tr>
                                    <td>A - {{ $pay->id }}</td>
                                    <td>{{ $pay->created_at }}</td>
                                    <td>$ {{ $pay->amount }}</td>
                                    <td>Pago de Abono</td>
                                    <td>ACTIVE</td>
                                    <td>{{ $sc->patient->name }}</td>
                                    <td class="text-center"><a href="{{ route('paymentReceipt', ['payment' => $pay->id]) }}" class="btn btn-link">Ver</a></td>

                                </tr>
                                @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>

    </div>
</div>

@endsection @section('scripts')
<script>
   $(document).ready(function() {
    $('#example').DataTable();
} );   
</script> @endsection