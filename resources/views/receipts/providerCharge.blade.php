@extends('layouts.default') @section('content') @section('page-header', 'Recibo') 
@section('page-header-small') 
Proveedores <br><br>
<div class="container">
    <div class="row">
        <div class="col-md-1">
            <a href="{{ route('viewProviderCharges', ['provider' => $charge->provider->id]) }}" class="btn btn-primary">Regresar a Recibos</a>
        </div>
        <div class="col-xs-1 col-xs-offset-9">
            <a id="toPrint" target="_blank" class="btn btn-primary">Imprimir
                <i class="fas fa-print"></i>
            </a>
        </div>
    </div>
</div>
@endsection


<style>
    .row div{
        padding-bottom: 8px;
    }
</style>

<div class="content container-fluid">
    <div class="row">
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-3">
                    <img src="/dist/img/logoUANL.jpg" width="100%">
                </div>
                <div class="col-xs-9">
                    <strong>UNIVERSIDAD AUTONOMA DE NUEVO LEON</strong>
                    <br> HOSPITAL UNIVERSITARIO "Dr. Jose Eleuterio Gonzalez"
                    <br> C.E.P.R.E.P.
                    <br> FICHA DE IDENTIFICACI&Oacute;N
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <img src="/dist/img/logoCEPREP.jpg" height="100px" align="right">
        </div>
    </div>
    <div class="row top-buffer justify-content-right">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 ><strong>RECIBO DE PROVEEDOR FOLIO #{{ $charge->id }}</strong></h3>
                    <h4><strong>Fecha de creaci&oacute;n:</strong> {{ $charge->created_at }}</h4>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
                        <thead>
                            <th>DATOS DEL PROVEEDOR</th>
                        </thead>
                        <tbody>
                            <td>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <strong>Nombre</strong>
                                        <br>{{ $charge->provider->name }}
                                    </div>
                                    <div class="col-xs-3">
                                        <strong>RFC</strong>
                                        <br> {{ $charge->provider->rfc }}
                                    </div>
                                    <div class="col-xs-3">
                                        <strong>Telefono:</strong>
                                        <br>{{ $charge->provider->telefono }}
                                    </div>
                                    <div class="col-xs-3">
                                        <strong>Direccion:</strong>
                                        <br>{{ $charge->provider->direccion }}
                                    </div>
                                </div>
                            </td>
                        </tbody>
                    </table>
                    <table class="table table-bordered">
                        <thead>
                            <th>DATOS DEL RECIBO</th>
                        </thead>
                        <tbody>
                            <td>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <strong>Concepto:</strong>
                                        <br>{{ $charge->additional_info }}
                                    </div>
                                    <div class="col-xs-6">
                                        <strong>Cantidad:</strong>
                                        <br> {{ $charge->total_amount }}
                                    </div>
                                </div>
                            </td>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection @section('scripts')
<script type="text/javascript">
    $('#toPrint').click(function () {
        window.print();
    });
</script> @endsection