@extends('layouts.default')

@section('page-header', 'Calendario')

@section('breadcrumb')
  <li><a href="#"><i class="fa fa-dashboard"></i> CEPREP Empleados</a></li>
  <li class="active">Calendario</li>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('bower_components/fullcalendar/dist/fullcalendar.min.css') }}">
<link rel="stylesheet" href="{{ asset('bower_components/fullcalendar/dist/fullcalendar.print.css') }}" media="print">
@endsection

@section('content')    
    <!-- Main content -->
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary"> 
          <div class="box-body">
              {{-- TABS DE DOCTORES Y PRUEBAS --}}
              <ul class="nav nav-tabs">
                <li class="active">
                  <a data-toggle="tab" href="#doctores-cal">Pruebas</a>
                </li>
                <li>
                  <a data-toggle="tab" href="#pruebas-cal">Consultas</a>
                </li>
              </ul>

              <div class="tab-content">
                <div id="doctores-cal" class="tab-pane fade in active">
                    <div class="row">
                        <div class="col-md-3">
                          
                            <div class="box box-solid">
                                <div class="box-header with-border">
                                  <h4 class="box-title">Tecnicos</h4>
                                </div>
                                <div class="box-body">
                                  <!-- the events -->
                                  <div id="external-events">
                                    <div class="external-event bg-fuchsia">
                                      Alexis: <input type="text" class="minibar pull-right" style="color:black">
                                    </div>
                                    <div class="external-event bg-yellow">Americo: <input class="minibar pull-right" type="text" style="color:black"></div>
                                    <div class="external-event bg-light-blue">Felipe: <input class="minibar pull-right" type="text" style="color:black"></div>
                                    <div class="external-event bg-gold">Armando: <input class="minibar pull-right" type="text" style="color:black"></div>
                                    <div class="external-event bg-newbluepurple">Poli Simple: <input class="minibar pull-right" type="text" style="color:black"></div>
                                    <div class="external-event bg-newred">Poli CPAP: <input class="minibar pull-right" type="text" style="color:black"></div>
                                  </div>
                                </div>
                                <!-- /.box-body -->
                              </div>
                              <!-- /. box -->
            
                          <div class="box box-solid">
                            <div class="box-header with-border">
                              <h4 class="box-title">Otros</h4>
                            </div>
                            <div class="box-body">
                              <!-- the events -->
                              <div id="external-events">
                                <div class="external-event bg-red">No agendar:
                                    <input type="hidden" value="No agendar">
                                </div>
                                
                              </div>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /. box -->
                          
                      </div><!-- /col-md-3 -->
            
                      <div class="col-md-9">
                        <div id="calendar-consultas"></div>
                      </div>
                      
            
                    </div>
                    <!--/row-->
                </div> <!-- DOCTORES CAL ENDS HERE -->

                <div id="pruebas-cal" class="tab-pane fade">
                    <div class="row">
                        <div class="col-md-3">
                          
                          <div class="box box-solid">
                            <div class="box-header with-border">
                              <h4 class="box-title">Doctores</h4>
                            </div>
                            <div class="box-body">
                              <!-- the events -->
                              <div id="external-events">
                                <div class="external-event bg-newpurple">Dr. Chavarria: <input class="minibar pull-right" type="text" style="color:black"></div>
                                <div class="external-event bg-reyes">Dr. Reyes: <input class="minibar pull-right" type="text" style="color:black"></div>
                                <div class="external-event bg-maroon">Dr. Sergio: <input class="minibar pull-right" type="text" style="color:black"></div>
                                <div class="external-event bg-olive">Dr. Erick R: <input class="minibar pull-right" type="text" style="color:black"></div>
                                <div class="external-event bg-newgray">Dra. Adriana: <input class="minibar pull-right" type="text" style="color:black"></div>
                                <div class="external-event bg-purple">Dr. Mercado: <input class="minibar pull-right" type="text" style="color:black"></div>
                                <div class="external-event bg-black">Residentes: <input class="minibar pull-right" type="text" style="color:black"></div>
                              </div>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /. box -->
            
                          <div class="box box-solid">
                            <div class="box-header with-border">
                              <h4 class="box-title">Otros</h4>
                            </div>
                            <div class="box-body">
                              <!-- the events -->
                              <div id="external-events">
                                <div class="external-event bg-red">No agendar:
                                    <input type="hidden" value="No agendar">
                                </div>
                                
                              </div>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /. box -->
                          
                      </div><!-- /.col-md-3 -->
            
                      
                      <div class="col-md-9">
                        <div id="calendar-pruebas"></div>
                      </div>
                      
            
            
                    </div><!--/row-->
                </div><!-- PRUEBAS CAL ENDS HERE-->
              </div>   
              
          </div>
        </div>
      </div>
    </div>
      
      
      <div id="appointment-info-modal-container"></div>
@endsection

@section('scripts')
<!-- General jquery functions-->
<script src="{{ asset('dist/js/general.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- fullCalendar -->
<script src="{{ asset('bower_components/moment/moment.js') }}"></script>
<script src="{{ asset('bower_components/fullcalendar/dist/fullcalendar.min.js') }}"></script>
<script type='text/javascript' src='{{ asset('bower_components/fullcalendar/dist/gcal.js') }}'></script>
<script src="{{ asset('dist/js/pages/calendar/appointments.js') }}"></script>
@endsection


    
