{{-- ***************************************************************************
    ***** NOTE: Cambiar metodo de mostrar modals pues es poco eficiente.   *****
    ***** No causa problemas por ahora pero despues hacer el cambio y usar *****
    ***** el de costs.blade.php                                            *****
    **************************************************************************** --}}
    
@extends('layouts.default')

@section('content')    

@section('page-header', 'Servicios del Hospital')
@section('page-header-small', 'ADMINISTRACIÓN')

<div class="content">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-add">
        Agregar nuevo servicio
    </button> <br><br>
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Lista de Servicios Actuales del Hospital</h3>
        </div>

        <div class="box-body">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <div class="modal fade @if($errors->newService->any()) in @endif" id="modal-add" @if($errors->newService->any()) style="display: block; padding-left: 0px;" @endif>
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Agregar nuevo servicio</h4>
                        </div>

                        @if($errors->newService->any())
                            <div class="alert alert-danger">
                                Se producieron los siguientes errores al intentar agregar al servicio:
                                <ul>
                                    @foreach ($errors->newService->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form action="{{ route('addNewService') }}" method="POST">
                        {{ csrf_field() }}
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>Nombre del Servicio:</label>
                                    <input type="text" class="form-control" value="{{ old('nombre') }}" name="nombre" placeholder=""/>
                                </div>

                                <div class="form-group">
                                    <label>Precio "K":</label>
                                    <input type="text" class="form-control" value="{{ old('precio_K') }}" name="precio_K" placeholder="9999.99" money_format('%i', $number)/>
                                </div>
                                
                                <div class="form-group">
                                    <label>Precio "C":</label>
                                    <input type="text" class="form-control" value="{{ old('precio_C') }}" name="precio_C" placeholder="9999.99"/>
                                </div>

                                <div class="form-group">
                                    <label>Precio "Profesor":</label>
                                    <input type="text" class="form-control" value="{{ old('precio_Profesor') }}" name="precio_Profesor" placeholder="9999.99"/>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar y cerrar</button>
                                <button type="submit" class="btn btn-primary">Agregar</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->


        
        <div class="col-md-12">
                @if ($errors->editService->any())
                    <div class="alert alert-danger">
                        Se producieron los siguientes errores al editar el servicio:
                        <ul>
                            @foreach ($errors->editService->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if ($errors->noPassword->any())
                    <div class="alert alert-danger">
                        Se necesita insertar la contraseña correcta para eliminar el servicio.
                    </div>
                @endif
                @if(session('editSuccess'))
                    <div class="alert alert-success">
                        Se edit&oacute; el servicio con &eacute;xito.
                    </div>
                @endif
                @if(session('deleteSuccess') === true)
                    <div class="alert alert-success">
                        Se eliminin&oacute; el servicio con &eacute;xito.
                    </div>
                @endif
                @if(session('deleteSuccess') === false)
                    <div class="alert alert-danger">
                        La contrase&ntilde;a insertada es incorrecta.
                    </div>
                @endif
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Estudio</th>
                            <th>Precio K</th>
                            <th>Precio C</th>
                            <th>Precio Profesor</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($services as $service)
                        <tr>
                            <td>{{ $service->id }}</td>
                            <td>{{ $service->name }}</td>
                            <td>$ {{ number_format($service->price_k, 2) }}</td>
                            <td>$ {{ number_format($service->price_c, 2) }}</td>
                            <td>$ {{ number_format($service->price_profesor, 2) }}</td>
                            <td>
                                <ul class="list-inline">
                                    
                                    <li class="list-inline-item">
                                        <button type="button" class="btn btn-link" data-toggle="modal" data-target="#modal-edit-{{ $service->id }}">
                                            <i class="fas fa-edit"></i> Editar
                                        </a>
                                    </li>

                                    <li class="list-inline-item">
                                        <button type="button" class="btn btn-link" data-toggle="modal" data-target="#modal-delete-{{ $service->id }}">
                                            <i class="fas fa-trash-alt"></i>Eliminar
                                        </a>
                                    </li>
                                </ul>
                            </td>
                        </tr>  

                        {{-- ESTE MODAL SALE CUANDO EL USUARIO CLICKEA EN EDITAR --}}
                        <div class="modal fade" id="modal-edit-{{ $service->id }}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Editar servicio</h4>
                                    </div>
                                    <form action="{{ route('editService') }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" value="{{ $service->id }}" name="id" />
                                        <div class="modal-body">

                                            <div class="form-group">
                                                <label>Nombre del Servicio:</label>
                                                <input type="text" class="form-control" value="{{ $service->name }}" name="nombre" placeholder=""/>
                                            </div>
        
                                            <div class="form-group">
                                                <label>Precio "K":</label>
                                                <input type="text" class="form-control" value="{{ $service->price_k }}" name="precio_K" placeholder="9999.99" money_format('%i', $number)/>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Precio "C":</label>
                                                <input type="text" class="form-control" value="{{ $service->price_c }}" name="precio_C" placeholder="9999.99"/>
                                            </div>
        
                                            <div class="form-group">
                                                <label>Precio "Profesor":</label>
                                                <input type="text" class="form-control" value="{{ $service->price_profesor }}" name="precio_Profesor" placeholder="9999.99"/>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar y cerrar</button>
                                            <button type="submit" class="btn btn-primary">Agregar</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->

                        {{-- ESTE MODAL SALE EL USER VA A BORRAR EL SERVICIO --}}
                        <div class="modal fade" id="modal-delete-{{ $service->id }}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Borrar Servicio</h4>
                                        </div>
                                        <form action="{{ route('deleteService') }}" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" value="{{ $service->id }}" name="id" />
                                            <div class="modal-body">
                                                &iquest;Seguro deseas eliminar el servicio?
                                                <div class="form-group">
                                                    <label>Inserta tu contrase&ntilde;a para continuar:</label>
                                                    <input type="password" class="form-control" name="password" placeholder="********"/>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar y cerrar</button>
                                                <button type="submit" class="btn btn-primary">Eliminar</button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->


                        @endforeach                 
                    </tbody>
                </table>
        
        </div>
    </div>
</div>
@endsection


@section('scripts')
<script>
   $(document).ready(function() {
    $('#example').DataTable();
} );   
</script>
@endsection