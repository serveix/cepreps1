@extends('layouts.default')
@section('page-header', 'Hoja de cobro')
@section('page-header-small', 'SERVICIOS')
@section('breadcrumb')
<li>
	<a href="#">
		<i class="fa fa-dashboard"></i>Hoja de cobro</a>
</li>
<li class="active">Nueva hoja de cobro de servicios</li>
@endsection
@section('content')
<div class="row">
	<div class="col-md-12">
		<form action="{{route('findAndShowPatient')}}" method="POST">
			{{ csrf_field() }}
		    <input name="searchedUserId" type="hidden" value="{{ $patient->id  }}"/>
		    <button class="btn btn-primary" type="submit">Regresar</button>
		</form>
	</div>
</div>

<div class="col-md-10 col-md-offset-1">	
	<div class="box box-primary">
		<div style="padding: 1em;">
			<div class="box-header with-border">
				<h3 class="box-title"><b>Paciente:</b> {{$patient->name}}</h3>
				<input id="patientId" value="{{$patient->id}}" class="hidden">
				
			</div>
			<!--Cuando la secretaria haya elegido a un paciente esta parte debera de aparecer-->
			<div class="box-body">
				<label>Elige los servicios</label>
				<div id="globalerrormsg" class="hidden alert alert-danger"> </div>
				<div id="globalsuccessmsg" class="hidden alert alert-success"> </div>
				<div id="services_fields" class="col-sm-12"> 
					<div class="form-group">
						<div id="serviceplus" class="form-group">
							<div class="input-group">
								<select class="form-control select2 serviceinput" name="services[]">
								<option value="0">Elige un Servicio.</option>
									@foreach($services as $service)
									<option value="{{$service->id}}"  pricec="{{$service->price_c}}" pricek="{{$service->price_k}}" priceprofesor="{{$service->price_profesor}}" class="service">
										{{ $service->name }}
									</option>
									@endforeach
								</select>
		
								<div class="input-group-btn">
									<button class="btn btn-success" type="button" onclick="services_fields();">
										<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
									</button>
								</div>
		
							</div>
						</div>
					</div>
					
					<div hidden class="form-group">
						<div id="serviceminus" class="input-group">
							<select class="form-control select2 serviceinput" name="services[]">
								<option value="0">Elige un Servicio.</option>
								@foreach($services as $service)
								<option value="{{$service->id}}" pricec="{{$service->price_c}}" pricek="{{$service->price_k}}" priceprofesor="{{$service->price_profesor}}" class="service">
									{{ $service->name }}
								</option>
								@endforeach
							</select>
						</div>
					</div>
				</div> <!--/services-fields-->
				<!---->
				<div hidden class="form-group">
					<div id="serviceminus" class="input-group">
						<select class="form-control select2 serviceinput" name="services[]">
						<option value="0">Elige un Servicio.</option>
							@foreach($services as $service)
							<option value="{{$service->id}}" pricec="{{$service->price_c}}" pricek="{{$service->price_k}}" priceprofesor="{{$service->price_profesor}}" class="service">
								{{$service->name}}
							</option>
							@endforeach
						</select>
					</div>
				</div>
	<!--=-=-=-=-=-==-=-=-=-=-=-=-=-RADIO BUTTONS=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-->
	<!--=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-->
				<div class="clear"></div>
				<div class="form-group">
					<label>Elige el costo</label>
					<div id="priceform" class="input-group">
						<div id="pricechoose">{{--era un form nspk--}}
							<label class="radio-inline"><input class="radiopricebtn" id="radiopricec" type="radio" value="pricec" name="optradio" checked="checked">Costo C</label>
							<label class="radio-inline"><input class="radiopricebtn" id="radiopricek" type="radio" value="pricek" name="optradio">Costo K</label>
							<label class="radio-inline"><input class="radiopricebtn" id="radiopricep" type="radio" value="pricep" name="optradio">Costo Profesor</label> 
							<label class="radio-inline hidden" id="label-pricespecial"><input class="radiopricebtn" id="radiopricespecial" type="radio" value="pricespecial" name="optradio">Costo especial</label> 
							<label class="radio-inline hidden" id="label-pricefree"><input class="radiopricebtn" id="radiopricefree" type="radio" value="pricefree" name="optradio">Cortesia</label> 
							<button id="specialprice" type="button" class="btn btn-link" data-toggle="modal" data-target="#specialpricemodal">Costo Especial</button>
						</div>
					</div>
				</div>
				{{--PRECIO ESPECIAL MODAL--}}
				@include('includes.modals.services.specialPrice')
				<div class="form-group">
					<label for="metodo-de-pago">M&eacute;todo de Pago</label>
					<select name="metodo-de-pago" class="form-control select2" id="metodo-de-pago">
						<option value="Efectivo">Efectivo</option>
						<option value="Tarjeta">Tarjeta</option>
						<option value="Cheque">Cheque</option>
					</select>
				</div>
				<div id="last-four" class="form-group hidden">
					<label>Ultimos 4 digitos del m&eacute;todo de pago</label>
					<div class="input-group">
						<div class="input-group-addon">
							<i class="fa fa-credit-card"></i>
						</div>
						<input id="numerosTarjeta" type="number" class="form-control"  placeholder="0000" maxlength="4">
					</div>
				</div>
				<div class="form-group">
					<label for="tipo-de-pago">Tipo de pago:</label>
					<select class="form-control select2" name="p" id="tipo-de-pago">
						<option value="1">Pago completo (una vez)</option>
						<option value="2">Con seguimiento</option>
						<option value="3">Pago completo con ayuda</option>
					</select>
				</div>
				<div id="amount-payed" class="form-group hidden">
					<label for="cantidad-pagada">Cantidad Pagada</label>
					<div class="input-group">
						<div class="input-group-addon">
							<i class="fa fa-dollar-sign"></i>
						</div>
						<input type="number" name="cantidad-pagada" id="cantidad-pagada" class="form-control" placeholder="0000.00">
					</div>
				</div>
				<div id="amount-payed-patient" class="form-group hidden">
					<label for="cantidad-pagada-paciente">Cantidad Pagada por Paciente</label>
					<div class="input-group">
						<div class="input-group-addon">
							<i class="fa fa-dollar-sign"></i>
						</div>
						<input type="number" name="cantidad-pagada-paciente" id="cantidad-pagada-paciente" class="form-control" placeholder="0000.00">
					</div>
				</div>
				{{--DESGLOSE DE COSTOS--}}
				<div id="chargedisplay" class="panel panel-info">
					<div class="panel-heading">
						<label style="font-size: 1.5em">Costos desglosados</label>
					</div>
					<div class="panel-body"> 
						<div id="pricelist"></div>
						<hr>
						<h4><strong>TOTAL:</strong> <span id="finalprice"></span></h4>
					</div>
				</div>

				<div class="form-group">
					<button id="btnconfirmcharge" type="button" class="btn btn-primary">Confirmar</button>
				</div>
				{{-- MODAL CONFIRMAR FINAL--}}
				@include('includes.modals.services.specialPriceFinal')
				@include('includes.modals.services.loadingApproval')
				@include('includes.modals.services.approved')
				@include('includes.modals.services.cancelled')
			</div>
		</div>
		<!-- /.row -->
	</div>
	<div id="xml">
		@foreach ($services as $service)
			<service class="serviceobject" id="{{$service->id}}" name="{{$service->name}}" pricec="{{$service->price_c}}" pricek="{{$service->price_k}}" priceprofesor="{{$service->price_profesor}}" pricespecial="{{$service->price_c}}" pricefree="0"></service>
		@endforeach
	</div>
	<direction id="directionURL" url='{{route('ChargeServicesOperation')}}'></direction>
	<!-- /.box-body -->
</div>
<!-- /.box -->

<!-- /.col -->
@endsection @section('scripts')

<script src="/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="/dist/js/pages/service_charge/ServiceCharge.js"></script>
<script src="/dist/js/pages/service_charge/actions.js"></script>
<script src="/dist/js/pages/service_charge/confirmation.js"></script>

@endsection