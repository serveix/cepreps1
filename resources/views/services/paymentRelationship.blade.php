@extends('layouts.default') @section('page-header', 'Recibos') @section('page-header-small', 'Buscar
Recibos y cuentas por cobrar') @section('content')

<?php 

    $cargo = 0;
    $abono = 0;
    $fila = 0;
?>

<div class="content">
    <div class="row"> 
        <div class="col-md-12">
            <form action="{{route('findAndShowPatient')}}" method="POST">
                {{ csrf_field() }}
                <input name="searchedUserId" type="hidden" value="{{ $patient->id  }}"/>
                <button class="btn btn-primary" type="submit">Regresar</button>
            </form>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h4>Paciente: {{$patient->name}}</h4>
            <div style="padding:1em">

                @if ($errors->any())
                      <div class="alert alert-danger">
                          Se producieron los siguientes errores al intentar agregar el abono:
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif

                  @if (session('status'))
                      <div class="alert alert-success">
                          {{ session('status') }}
                      </div>
                  @endif
                
                @foreach ($servicesCharges as $sc)
                
                <?php $cargo = 0; $abono = 0; ?>
                @if($fila == 0) <div class="row"> @endif
                <?php $fila++; ?>
                <div style="padding:6px" class="col-md-4 col-sm-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="background-color: #9b4a89; color:white">
                            Hoja de Cobro N° {{$sc->id}} Fecha: {{$sc->created_at}}
                        </div>
                        <!-- Table -->
                        <table class="table table-hover table-condensed">
                            <thead>
                                <tr class="active text-info">
                                    <th>
                                        <p>ID</p>
                                    </th>
                                    <th>
                                        <p>Servicios</p>
                                    </th>
                                    <th style="min-width: 72px;">
                                        <p class="text-right">Costo</p>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($sc->services as $s)
                                <tr>
                                    <td>S-{{ $s->id }}</td>
                                    <td>{{ $s->name }}</td>
                                    <td class="text-right text-info">$ {{ $s->pivot->price }}</td>
                                    <?php $cargo = $cargo + $s->pivot->price ?>
                                </tr>
                                @endforeach
                            </tbody>
                            <thead>
                                <tr class="active text-success">
                                    <th>
                                        <p>ID</p>
                                    </th>
                                    <th>
                                        <p>Abono / Pago</p>
                                    </th>
                                    <th>
                                        <p class="text-right">Cantidad</p>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td>Primer Pago:</td>
                                    <td class="text-right text-success">$ {{ $sc->total_amount }}</td>
                                    <?php $abono = $abono + $sc->total_amount ?>
                                </tr>
                                @foreach($sc->payments as $pay)
                                <tr>
                                    <td>A-{{$pay->id}}</td>
                                    <td>Emitido: {{$pay->created_at}}</td>
                                    <td class="text-right text-success">$ {{$pay->amount}}</td>
                                    <?php $abono = $abono + $pay->amount ?>
                                </tr>
                                @endforeach
                                <?php $total = round($cargo - $abono, 2) ?>
                                <tr class="@if($total <= 0) success text-success @else danger text-danger @endif">
                                    <td></td>
                                    <td>
                                        <b>Total / Restante de Pago</b>
                                    </td>
                                    <td class="text-right">
                                    
                                        <b>$ {{ $total }}</b>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="panel-footer panel-primary" style="background-color: #9b4a89; color:white">
                            @if($abono < $cargo) 
                            <a class=" btn btn-default btn-sm" data-toggle="modal" href='#modal-{{$sc->id}}'>Abonar</a>
                            @endif
                        </div>
                    </div>
                </div>
                @if($fila == 3) </div> <?php $fila = 0; ?> @endif
                @if($abono < $cargo) 
                <div class="modal fade" id="modal-{{$sc->id}}">
                    <div class="modal-dialog">
                        <form action="{{ route('createPay') }}" method="POST">
                            
                                {{ csrf_field() }}

                            <div class="modal-content">
                                <div class="modal-header" style="background-color: #9b4a89; color:white">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Abono a la Hoja de Cobro N°{{$sc->id}} - Fecha: {{$sc->created_at}}</h4>
                                </div>
                                <div class="modal-body">
                                    <input type="hidden" name="patient" value="{{$patient->id}}">
                                    <input type="hidden" name="serviceCharge" value="{{$sc->id}}">
                                    <input type="hidden" name="total_pay" value="{{$abono}}">
                                    <input type="hidden" name="total_charge" value="{{$cargo}}">
                                    <div class="form-group">
                                        <label for="cantidad">Cantidad de Abono:</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-usd"></i>
                                            </span>
                                            <input type="number" step="any" class="form-control" name="cantidad_de_abono" id="amount_{{$sc->id}}" value="{{(int)$cargo - $abono}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tipo_pago">Tipo de pago:</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-credit-card"></i>
                                            </span>
                                            <select class="form-control select2" name="tipo_de_pago" id="payment_method_{{$sc->id}}" onchange="paymentMethod({{$sc->id}})">
                                                <option value="Efectivo">Efectivo</option>
                                                <option value="Tarjeta">Tarjeta</option>
                                                <option value="Cheque">Cheque</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group hidden" id="credit-card-{{$sc->id}}">
                                        <label for="credit_card">Número tarjeta de crédito (últimos 4 dígitos):</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-credit-card"></i>
                                            </span>
                                            <input type="number" name="tarjeta" class="form-control" id="four_numbers_{{$sc->id}}" placeholder="0000">
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                    <button type="sumbit" class="btn btn-primary">Guardar Cambios</button>
                                </div>
                            </div>
                        </form>
                    </div>
            </div>
            @endif @endforeach
        </div>
    </div>
</div>
</div>

@endsection @section('scripts')
<script>
    $(document).ready(function () {
        
    });
    function paymentMethod(id) {
        val = $('#payment_method_' + id + ' option:selected').val();
        if (val == 'Efectivo') {
            $('#credit-card-' + id + '').addClass('hidden');
        } else {
            $('#credit-card-' + id + '').removeClass('hidden');
        }
    }
</script> @endsection