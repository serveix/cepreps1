@extends('layouts.default') @section('content') @section('page-header', 'Recibos') @section('page-header-small', 'Buscar
Recibos y cuentas por cobrar')
<div class="content">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <form action="{{route('findAndShowPatient')}}" method="POST">
                        {{ csrf_field() }}
                        <input name="searchedUserId" type="hidden" value="{{ $patient->id  }}"/>
                        <button class="btn btn-primary" type="submit">Regresar</button>
                    </form><br>
                    <h4 class="box-title">Paciente: {{$patient->name}}</h4>
                </div>
                @if(session('cancelled'))
                <div class="alert alert-success">
                    La hoja de cobro fue cancelada.
                </div>
                @endif
                <div class="box-body">
                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Folio</th>
                                <th>Fecha</th>
                                <th>Cantidad</th>
                                <th>Tipo</th>
                                <th>Estado de Aprobaci&oacute;n</th>
                                <th>Acci&oacute;nes</th>
                            </tr>
                        </thead>
                        <tbody>

                        <?php 
                        $servicesCharges = $patient->servicesCharges;
                        // $servicesCharges = $servicesCharges->where('billing_status', '!=', 'CANCELLED')->where('approval_status', '!=', 'CANCELLED');
                        ?>

                            @foreach($servicesCharges as $sc)
                            <tr>
                                <td>C - {{ $sc->id }}</td>
                                <td>{{ $sc->created_at }}</td>
                                <td>$ {{ $sc->total_amount }}</td>
                                <td>Cargo de Servicio</td>
                                <td>{{ $sc->approval_status }}</td>
                                <td class="text-center">
                                    <form action="{{ route('cancelServicesCharge') }}" method="POST">
                                        
                                        <a href="{{ route('serviceChargeReceipt', ['servicesCharge' => $sc->id]) }}" class="btn btn-link">Ver</a> 
                                        
                                        @if(Auth::user()->hasPermission('ADMIN_CANCEL_RECEIPTS') == 'yes')
                                        {{ csrf_field() }}
                                        <input type="hidden" name="scId" value="{{ $sc->id }}">
                                        <button type="submit" class="btn btn-link">Cancelar</button>
                                        @endif
                                        
                                    </form>
                                </td>
                            </tr>
                                @foreach($sc->payments as $pay)
                                <tr>
                                    <td>A - {{ $pay->id }}</td>
                                    <td>{{ $pay->created_at }}</td>
                                    <td>$ {{ $pay->amount }}</td>
                                    <td>Pago de Abono</td>
                                    <td>ACTIVE</td>
                                    <td class="text-center"><a href="{{ route('paymentReceipt', ['payment' => $pay->id]) }}" class="btn btn-link">Ver</a></td>

                                </tr>
                                @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div><!--box-body -->
                
            </div> <!--box box-primary-->
        </div><!--col col-md-12 -->

    </div><!-- row -->
</div> <!-- content -->

@endsection @section('scripts')
<script>
   $(document).ready(function() {
    $('#example').DataTable();
} );   
</script> @endsection