@extends('layouts.default')

@section('page-header', 'Hoja de cobro')

@section('page-header-small', 'DOCTOR')

@section('breadcrumb')
  <li><a href="#"><i class="fa fa-dashboard"></i>Hoja de cobro</a></li>
  <li class="active">Nueva hoja de cobro de doctor</li>
@endsection

@section('content')    

<!-- SELECT2 EXAMPLE -->
<div class="box box-default">
<div class="box-header with-border">
  <h3 class="box-title">Empleado: John Doe</h3>
</div>

<!-- /.box-header -->
<div class="box-body">
  <div class="row">
	    <div class="col-md-8">
	      	<div class="form-group">
	            <label>Doctor</label>
	            <select class="form-control select2" style="width: 100%;">
	              <option selected="selected">name_1</option>
	              <option>name_2</option>
	              <option>name_3</option>
	              <option>name_4</option>
	              <option>name_5</option>
	              <option>name_6</option>
	              <option>name_7</option>
	            </select>
	         </div>   
	    </div> 
	    <!-- /.col -->

	    <div class="col-md-8">
	    	 <!-- select -->
	        <div class="form-group">
	          <label>Servicio</label>
	          <select class="form-control">
	            <option>service_1</option>
	            <option>service_2</option>
	            <option>service_3</option>
	            <option>service_4</option>
	            <option>service_5</option>
	          </select>
	      		<button type="button" class="btn btn-success pull-right">+</button>
	        </div>  
		</div>
		<!-- /.col -->	

		<div class="col-md-8">
			<div class="form-group">
                  <label>Precio</label>
                  <div class="input-group">
	                <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
	                <input type="text" class="form-control">
	              </div>
                </div> 
		</div>

		<div class="col-md-8">
			<div class="form-group">
                 <button type="button" class="btn btn-primary pull-right" onclick="myFunction()">Confirmar</button> 
            </div> 
		</div>
	</div>
	 <!-- /.row --> 
</div>
<!-- /.box-body -->   
</div>
      <!-- /.box -->

@endsection

@section('scripts')

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
  })
</script>

<script>

</script>

<script>
function myFunction() {
    window.print();
}
</script>

@endsection