@extends('layouts.default')

@section('content')    
@section('page-header', 'Acreedores')
@section('page-header-small', 'AGREGAR NUEVO')

<div class="content">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Datos del acreedor</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                
                  <div class="box-body">
                  @if ($errors->any())
                      <div class="alert alert-danger">
                          Se producieron los siguientes errores al intentar agregar al acreedor:
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif

                  @if (session('status'))
                      <div class="alert alert-success">
                          {{ session('status') }}
                      </div>
                  @endif

                  <form id="addNewSupplier" action="{{ route('storeSupplier') }}" method="POST">
                    
                    {{ csrf_field() }}

                    <div class="form-group">
                      <label for="p-name">Nombre</label>
                      <input type="text" name="nombre" value="{{ old('nombre') }}" class="form-control" id="p-name" placeholder="Juan Isai">    
                    </div>
                    
                    
                  <div class="box-footer">
                    <!-- <button type="submit" class="btn btn-default">Cancelar</button> -->
                    <button type="submit" class="btn btn-primary pull-right">Agregar</button>
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('scripts')
<!-- InputMask -->
<script src="/plugins/input-mask/jquery.inputmask.js"></script>
<script src="/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
  //Money Euro
    // $('[data-mask]').inputmask();
    $('.select2').select2();

    $('form#addNewSupplier').submit(function(){
      $(this).find(':input[type=submit]').prop('disabled', true);
    });
</script>
@endsection