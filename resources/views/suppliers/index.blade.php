@extends('layouts.default')

@section('content')    

@section('page-header', 'Proveedores')
@section('page-header-small', 'ADMINISTRACIÓN')

<div class="content">
    @if(session('success'))
    <div class="alert alert-success">
        Se elimino el usuario.
    </div>
    @endif
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Lista de Proveedores</h3>
        </div>
        <div class="box-body">
            <table id="suppliers" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($suppliers as $supplier)
                    <tr>
                        <td>{{ $supplier->id }}</td>
                        <td>{{ $supplier->name }}</td>
                        <td>
                            <form method="POST" action="{{route('destroySupplier', ['id' => $supplier->id])}}">
                                {{csrf_field()}}
                                <button type="submit" class="btn btn-link">
                                    <i class="fas fa-trash-alt"></i> Eliminar
                                </button>
                            </form>
                        </td>
                    </tr>    
                    @endforeach             
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection


@section('scripts')
<script>
   $(document).ready(function() {
    $('#suppliers').DataTable();
} );   
</script>
@endsection