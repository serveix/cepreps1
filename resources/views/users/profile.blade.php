@extends('layouts.default')

@section('page-header', 'Perfil')

@section('breadcrumb')
  <li><a href="#"><i class="fa fa-dashboard"></i> Ceprep Empleados</a></li>
  <li class="active">Perfil</li>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('bower_components/fullcalendar/dist/fullcalendar.min.css') }}">
<link rel="stylesheet" href="{{ asset('bower_components/fullcalendar/dist/fullcalendar.print.min.css') }}" media="print">
@endsection

@section('content')    
    <!-- Main content -->
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <!--
            <div class="box box-primary">
            <div class="box-body no-padding">
            -->
                <div class="col-md-12" >
                    <div class="panel panel-info">
                        <div class="panel-heading">
                        <h3 class="panel-title">Datos del empleado</h3>
                        </div>
                        <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="dist/img/user2-160x160.jpg" class="img-circle img-responsive"> </div>
                            <div class=" col-md-9 col-lg-9 "> 
                                <table class="table table-user-information">
                                    <tbody>
                                        <tr>
                                            <td>Nombre:</td>
                                            <td>{{ Auth::user()->name }}</td>
                                        </tr>
                                        <br>
                                        <tr>
                                            <td>Email:</td>
                                            <td>{{ Auth::user()->email }}</td>
                                        </tr>
                                        <br>
                                        <tr>
                                            <td>Miembro desde:</td>
                                            <td>{{ Auth::user()->created_at }}</td>
                                        </tr>  
                                        <tr>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div>
                        <div class="panel-footer">
                        </div>       
                    </div>
        </div>
            </div>
          </div>
        </div>
      </div>
@endsection

@section('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- fullCalendar -->
<script src="{{ asset('bower_components/moment/moment.js') }}"></script>
<script src="{{ asset('bower_components/fullcalendar/dist/fullcalendar.min.js') }}"></script>
<!-- Scripts del calendario y sus eventos -->
<script src="{{ asset('dist/js/pages/calendar/appointments.js') }}"></script>
@endsection