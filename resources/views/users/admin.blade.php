@extends('layouts.default')

@section('content')    

@section('page-header', 'Empleados')
@section('page-header-small', 'ADMINISTRACIÓN')

<div class="content">
    @if(session('success'))
    <div class="alert alert-success">
        Se elimino el usuario.
    </div>
    @endif
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Usuarios de Empleados Sistema CEPREP</h3>
        </div>
        <div class="box-body">
            <table id="users" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre completo</th>
                        <th>Correo electronico</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            <form method="POST" action="{{route('deleteUser')}}">
                                {{csrf_field()}}
                                <input type="hidden" name="userId" value="{{$user->id}}">
                                <button type="submit" class="btn btn-link">
                                    <i class="fas fa-trash-alt"></i> Eliminar
                                </button>
                            </form>
                        </td>
                    </tr>    
                    @endforeach             
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection


@section('scripts')
<script>
   $(document).ready(function() {
    $('#users').DataTable();
} );   
</script>
@endsection