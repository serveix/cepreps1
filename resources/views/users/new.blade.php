@extends('layouts.default')

@section('content')    
@section('page-header', 'Usuarios')
@section('page-header-small', 'AGREGAR NUEVO')

<div class="content">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Agregar usuario</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputName" class="col-sm-2 control-label">Nombre</label>

                      <div class="col-sm-10">
                        <input type="name" class="form-control" id="inputName" placeholder="Nombre">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputLastName" class="col-sm-2 control-label">Apellido</label>

                      <div class="col-sm-10">
                        <input type="name" class="form-control" id="inputLastName" placeholder="Apellido">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputTel" class="col-sm-2 control-label">Teléfono</label>

                      <div class="col-sm-10">
                        <input type="tel" class="form-control" id="inputTel" placeholder="00-0000-0000">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Email</label>

                      <div class="col-sm-10">
                        <input type="email" class="form-control" id="inputEmail3" placeholder="johndoe@hotmail.com">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputBirthDay" class="col-sm-2 control-label">Fecha de nacimiento</label>

                      <div class="col-sm-10">
                        <input type="date" class="form-control" id="inputBirthDay" placeholder="DD/MM/AA">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputText" class="col-sm-2 control-label">Referencias</label>

                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputText" placeholder="Referencia">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputResidence" class="col-sm-2 control-label">Domicilio </label>

                      <div class="col-sm-3">
                        <input type="text" class="form-control" id="inputResidence" placeholder="Calle: Zafiro">
                      </div>
                      <div class="col-sm-3">
                        <input type="text" class="form-control" id="inputResidence" placeholder="Núm: #900">
                      </div>
                      <div class="col-sm-3">
                        <input type="text" class="form-control" id="inputResidence" placeholder="Colonia: Las Puentes">
                      </div>
                    </div>
                    

                    

                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-default">Cancelar</button>
                    <button type="submit" class="btn btn-primary pull-right">Agregar</button>
                  </div>
                  <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </div>
</div>

@endsection


@section('scripts')

@endsection