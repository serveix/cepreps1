@extends('layouts.default')
@section('page-header', 'Aprobaci&oacute;nes')
@section('page-header-small', 'COSTOS ESPECIALES')
@section('breadcrumb')
<li>
	<a href="#">
		<i class="fas fa-thumbs-up"></i> Aprobaciones</a>
</li>
<li class="active">Costos</li>
@endsection
@section('content')
<div class="content">
	<div class="box box-primary">
		<div class="box-header">
			<h3 class="box-title">Hojas de Cobro Pendientes de Aprobaci&oacute;n</h3>
		</div>
        <div class="box-body">
			@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			@if(session('approveSuccess') === false)
			    <div class="alert alert-danger">
					La contrase&ntilde;a introducida es incorrecta
				</div>
			@elseif(session('approveSuccess') === true)
			    <div class="alert alert-success">
					La hoja de cobro fue aprobada.
				</div>
			@endif
			@if(session('cancelSuccess') === false)
			    <div class="alert alert-danger">
					La contrase&ntilde;a introducida es incorrecta
				</div>
			@elseif(session('cancelSuccess') === true)
			    <div class="alert alert-success">
					La hoja de cobro fue cancelada.
				</div>
			@endif
			<table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Folio #</th>
						<th>Fecha Generada</th>
						<th>Paciente</th>
						<th>Accion</th>
					</tr>
				</thead>
				
				<tbody>
					@foreach($servicesCharges as $sc)
					<tr>
						<td>{{ $sc->id }}</td>
						<td>{{ $sc->created_at }}</td>
						<td>{{ $sc->patient->name }}</td>
						<td>
							<ul class="list-inline">
								<li class="list-inline-item">
									<button class="btn btn-link" onclick="openModal( {{ $sc->id }}, 'details' )">
										<i class="fas fa-info-circle"></i> 
										Ver Detalles
									</button>
								</li>
								
								<li class="list-inline-item">
									<button class="btn btn-link" onclick="openModal( {{ $sc->id }}, 'approve')">
										<i class="fas fa-thumbs-up"></i> 
										Aprobar
									</button>
								</li>

								<li class="list-inline-item">
									<button class="btn btn-link" onclick="openModal( {{ $sc->id }}, 'cancel')">
										<i class="fas fa-times"></i> 
										Cancelar
									</button>
								</li>
							</ul>
						</td>
					</tr>
					@endforeach    
				</tbody>
			</table>
            
	    </div>
	</div>
	
	<div id="showing-modal"> {{-- Modal HTML gets here by JQuery --}} </div>
	
</div>
@endsection 
@section('scripts')
<script>
	/**
	 * Envia un request a la ruta con nombre getApprovalModal
	 * para que regrese HTML del modal necesario. (Detalles, aprobacion o 
	 * cancelacion)
	 */
	function getModalData(dynamicData) {
		return $.ajax({
		    url: "{{ route('getApprovalModal') }}",
		    type: "get",
		    data: dynamicData
		});
	}
	
	function openModal(id, modalAction) {
		
		dynamicData = {
			id: id,
			modalAction: modalAction,
		};

		getModalData(dynamicData).done(function(result) {
			
			$('#showing-modal').html(result);
			$('#approval-modal').modal('toggle'); // approval-modal es el modal generado segun lo que clickeo
		});
		
	}

	$(document).ready(function() {
	   $('#dataTable').DataTable();
    });   
</script>
@endsection