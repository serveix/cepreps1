<!-- APPROVE MODAL -->
<div class="modal fade" id="appointment-info-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#9b4a89; color:white">
                <button type="button" style="color:white" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{{$appointment->title}} ({{$appointment->start}}) </h4>
            </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success hidden" id="success-label"></div>
                            <form id="descForm">
                                <span style="font-size:1rpt;">
                                    <label>Detalles:</label> 
                                </span>
                                <textarea id="desc" class="form-control">{{$appointment->description or 'No hay detalles extra' }}</textarea>
                                <br>
                                <button  class="btn btn-primary"> 
                                    Actualizar
                                </button>
                            </form>
                        </div> 
                    </div>
                </div>
                <div class="modal-footer">
                    <form action="{{route('deleteAppointment')}}" method="POST">
                        {{ csrf_field() }}
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        
                        <input type="hidden" name="appointmentId" value="{{ $appointment->id }}"> 
                        <button type="submit" class="btn btn-primary">Eliminar evento</button>
                    </form>
                </div>
            
        </div>
    </div>
</div>