<!-- DETAILS MODAL-->
<div class="modal fade" id="approval-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="background-color:#9b4a89; color:white">
                    <button type="button" style="color:white" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Detalles por aprobar - Hoja de Cobro #{{$servicesCharge->id}}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Servicios</h4>
                            
                            <table class="table">
                                <thead>
                                    <th>Nombre del Servicio</th>
                                    <th>Precio solicitado</th>
                                </thead>
                                <tbody>
                                @php $totalAmount = 0; @endphp
                                @foreach($servicesCharge->services as $s)
                                    <tr>
                                        <td>{{$s->name}}</td>
                                        <td>{{$s->pivot->price}}</td>
                                    </tr>
                                @php $totalAmount += $s->pivot->price; @endphp
                                @endforeach
                                <tbody>
                            </table>
                            <strong>Cantidad total: </strong> $ {{ $totalAmount }}<br>
                            <strong>Cantidad pagada: </strong> $ {{$servicesCharge->total_amount}}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>