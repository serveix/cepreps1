<!-- APPROVE MODAL -->
<div class="modal fade" id="approval-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="background-color:#9b4a89; color:white">
                    <button type="button" style="color:white" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Cancelaci&oacute;n - Hoja de Cobro # {{ $servicesCharge->id }}</h4>
                </div>
                <form action="{{ route('cancelCosts') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                Para cancelar la hoja de cobro, verifique su inicio de sesi&oacute;n.
                                <div class="form-group">
                                    <label for="contrasena">Contraseña:</label>
                                    <input class="form-control" type="password" id="contrasena" name="password" placeholder="******">
                                    <input type="hidden" name="servicesChargeId" value="{{ $servicesCharge->id }}">
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button id="confirm-request" type="submit" class="btn btn-primary">Confirmar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>