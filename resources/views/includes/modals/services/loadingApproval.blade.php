<!-- MODAL LOADING APPROVAL -->
<div class="modal fade" id="loading-approval">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#9b4a89; color:white">
                <h4 class="modal-title">Esperado autorizaci&oacute;n</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                        Se ha enviado una solicitud de precio especial a los usuarios correspondientes. Por favor, espere mientras
                        se aprueba.<br>
                        <img src="{{ asset('dist/img/loading.gif') }}" height="100px" alt="Cargado..."><br>
                        <button onclick="location.reload()" id="skip-waiting" type="button" class="btn btn-primary">Dejar en pendiente</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>