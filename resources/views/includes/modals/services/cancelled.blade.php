<!-- MODAL CANCELLED SERVICES CHARGE -->
<div class="modal fade" id="cancelled-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#9b4a89; color:white">
                <button type="button" style="color:white" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Costos especiales aprobados</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                            <div class="alert alert-danger">
                                Se ha cancelado la hoja de cobro de servicio.
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>