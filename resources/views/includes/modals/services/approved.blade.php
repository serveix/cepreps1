<!-- MODAL APPROVED SERVICES CHARGE -->
<div class="modal fade" id="approved-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#9b4a89; color:white">
                <button type="button" style="color:white" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Costos especiales aprobados</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                            <div class="alert alert-success">
                                Se han aprobado los costos especiales solicitada.
                                Se le redireccionara a los detalles de la hoja de cobro.
                            </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="url-button">Abrir Recibo</button>
            </div>
        </div>
    </div>
</div>