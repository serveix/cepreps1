<div class="modal fade" id="specialpricemodal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="background-color:#9b4a89; color:white">
                    <button type="button" style="color:white" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 id="specialpricemodaltitle" class="modal-title">Precio Especial</h4>
                </div>
                <div class="modal-body">
                    <div class="">
                        <div class="btn-group btn-group-justified">
                            <a id="modalspecialpricebtn" type="button" class="btn btn-primary">Precio Especial</a>
                            <a id="modalfreepricebtn" type="button" class="btn btn-default">Cortesía</a>
                        </div>
                        <!--lista de errores-->
                        <div class="alert alert-danger autherrors hidden">
                                <p id="autherrorstext"></p>
                        </div>
                        <div style="padding: 1em;">
                            <div id="successauthorization">
                                <div id="modalbodyspecialprice" class="modalpricebody">
                                    <div id="modalspecialpriceform" class="form-horizontal">
    
                                    </div>
                                </div>
    
                                <div id="modalbodyfreeprice" class="hidden modalpricebody" style="padding: 2em;">
                                    <div class="alert alert-success" style="margin: 1em;">
                                        <h3 class="text-center">La cortesía hará que sus servicios médicos consumidos sean totalmente gratis.<br></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button id="btnsavespecialprice" type="button" class="btn btn-primary">Guardar cambios</button>
                </div>
            </div>
        </div>
    </div>