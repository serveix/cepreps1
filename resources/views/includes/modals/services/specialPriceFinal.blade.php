<!-- MODAL CONFIRMAR FINAL -->
<div class="modal fade" id="specialpricefinal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#9b4a89; color:white">
                <button type="button" style="color:white" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Autorizaci&oacute;n Costo Especial</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        Se enviara la solicitud de aprobaci&oacute;n de costo especial.
                        &iquest;Desea continuar?
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button id="confirm-request" type="button" class="btn btn-primary">Continuar</button>
            </div>
        </div>
    </div>
</div>