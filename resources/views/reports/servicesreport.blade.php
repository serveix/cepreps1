<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/dist/css/styles.css">
    
</head>
<body onload="window.print()">
    {{-- <button onclick="window.print()" class="no-print btn btn-default">Imprimir</button> --}}
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <div class="row">
                    <div class="col-xs-3">
                        <img src="/dist/img/logoUANL.jpg" width="100%">
                    </div>
                    <div class="col-xs-9">
                        <strong>UNIVERSIDAD AUTONOMA DE NUEVO LEON</strong>
                        <br> HOSPITAL UNIVERSITARIO "Dr. Jose Eleuterio Gonzalez"
                        <br> C.E.P.R.E.P.
                        <br> FICHA DE IDENTIFICACI&Oacute;N
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <img src="/dist/img/logoCEPREP.jpg" height="100px" align="right">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h2>Servicios del Mes ({{ $servicesCharges->first()->created_at->month }}/{{$servicesCharges->first()->created_at->year }})</h2>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th colspan="3">
                                <h4>Precio K</h4>
                            </th>
                        </tr>
                        <tr>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Cantidad
                            </th>
                            <th>
                                Precio
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($priceK as $service)
                        <tr>
                            <td>
                                {{ $service[0]->name }}
                            </td>
                            <td>
                                {{ $service->count() }}
                            </td>
                            <td>
                                {{ $service[0]->pivot->price }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th colspan="3">
                                <h4>Precio C</h4>
                            </th>
                        </tr>
                        <tr>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Cantidad
                            </th>
                            <th>
                                Precio
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($priceC as $service)
                        <tr>
                            <td>
                                {{ $service[0]->name }}
                            </td>
                            <td>
                                {{ $service->count() }}
                            </td>
                            <td>
                                {{ $service[0]->pivot->price }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th colspan="3">
                                <h4>Precio Profesor</h4>
                            </th>
                        </tr>
                        <tr>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Cantidad
                            </th>
                            <th>
                                Precio
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($priceProfesor as $service)
                        <tr>
                            <td>
                                {{ $service[0]->name }}
                            </td>
                            <td>
                                {{ $service->count() }}
                            </td>
                            <td>
                                {{ $service[0]->pivot->price }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th colspan="3">
                                <h4>Cortesias</h4>
                            </th>
                        </tr>
                        <tr>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Cantidad
                            </th>
                            <th>
                                Precio
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($priceCortesy as $service)
                        <tr>
                            <td>
                                {{ $service[0]->name }}
                            </td>
                            <td>
                                {{ $service->count() }}
                            </td>
                            <td>
                                {{ $service[0]->pivot->price }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th colspan="3">
                                <h4>Precio Especial</h4>
                            </th>
                        </tr>
                        <tr>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Cantidad
                            </th>
                            <th>
                                Precio
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($priceSpecial as $service)
                        <tr>
                            <td>
                                {{ $service[0]->name }}
                            </td>
                            <td>
                                {{ $service->count() }}
                            </td>
                            <td>
                                {{ $service[0]->pivot->price }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>
