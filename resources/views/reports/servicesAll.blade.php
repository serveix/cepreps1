@extends('layouts.default')
@section('page-header', 'Reportes')
@section('page-header-small', 'Servicios')
@section('breadcrumb')
<li>
	<a href="#">
		<i class="fa fa-file-alt"></i> Reportes</a>
</li>
<li class="active">Servicios</li>
@endsection
@section('content')
<div class="content">
	<div class="box box-primary">
		<div class="box-header">
			<h3 class="box-title">Elige el mes para generar el reporte</h3>
		</div>
        <div class="box-body">
            <form action="{{ route('generateServicesAllReport') }}" method="POST" target="reporte">
                {{ csrf_field() }}

                <div class="form-group">
                    <label>Meses disponibles: </label>
                    <select class="form-control" name="date">
                        @foreach($dates_avai as $date)
                        <option value="{{$date}}">{{$date}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <input type="submit" value="Generar" type="submit" class="btn btn-primary">
                </div>
            </form>
            
            <iframe name="reporte" style="width:0;height:0" frameborder="0" ></iframe>
	    </div>
	</div>
	
	
	
</div>
@endsection 
@section('scripts')
<script>
	 
</script>
@endsection