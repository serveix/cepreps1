<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/dist/css/styles.css">
    
</head>
<body onload="window.print()">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <div class="row">
                    <div class="col-xs-3">
                        <img src="/dist/img/logoUANL.jpg" width="100%">
                    </div>
                    <div class="col-xs-9">
                        <strong>UNIVERSIDAD AUTONOMA DE NUEVO LEON</strong>
                        <br> HOSPITAL UNIVERSITARIO "Dr. Jose Eleuterio Gonzalez"
                        <br> C.E.P.R.E.P.
                        <br> FICHA DE IDENTIFICACI&Oacute;N
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <img src="/dist/img/logoCEPREP.jpg" height="100px" align="right">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h2>Recibos del Mes ({{ $servicesCharges->first()->created_at->month }}/{{$servicesCharges->first()->created_at->year }})</h2>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th colspan="3">
                                <h4>Todos los recibos</h4>
                            </th>
                        </tr>
                        <tr>
                            <th>
                                Folio
                            </th>
                            <th>
                                Fecha
                            </th>
                            <th>
                                Titular
                            </th>
                            <th>
                                Precio Total
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($servicesCharges as $sc)
                        <tr>
                            <td>
                                {{ $sc->id }}
                            </td>
                            <td>
                                {{ $sc->created_at }}
                            </td>
                            <td>
                                {{ $sc->patient->name or $sc->provider->name }}
                            </td>
                            <td>
                                {{ $sc->total_amount }}
                            </td>
                        </tr>
                        @endforeach
                        <tr>
                            <td colspan="3" class="text-right"><strong>Total:</strong> </td>
                            <td>{{$total}}</td>
                        </tr>
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>
</body>
</html>
