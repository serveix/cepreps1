@extends('layouts.default')

@section('content')    
@section('page-header', 'Cheques')
@section('page-header-small', 'GENERAR NUEVO')

<div class="content">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Datos del cheque</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                
                  <div class="box-body">
                  @if ($errors->any())
                      <div class="alert alert-danger">
                          Se producieron los siguientes errores al intentar agregar al paciente:
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif

                  @if (session('status'))
                      <div class="alert alert-success">
                          {{ session('status') }}
                      </div>
                  @endif

                  <form id="addNewCheck" action="{{ route('storeCheck') }}" method="POST">
                    
                    {{ csrf_field() }}

                    <div class="form-group">
                      <label for="amount">Cantidad</label>
                      <input type="text" name="cantidad" value="{{ old('cantidad') }}" class="form-control" id="amount" placeholder="0.00">    
                    </div>

                    <div class="form-group">
                      <label for="concept">Concepto</label>
                      <input type="text" name="concepto" value="{{ old('concepto') }}" class="form-control" id="concept" placeholder="Concepto de cheque">    
                    </div>

                    <div class="form-group">
                      <label for="invoice-no">N&uacute;mero de factura <i><small>(Opcional)</small></i></label>
                      <input type="text" name="numero_de_factura" value="{{ old('numero_de_factura') }}" class="form-control" id="invoice-no" placeholder="Numero de factura relacionada">
                    </div>

                    <div class="form-group">
                      <label for="checkbook">Chequera</label>
                      <select class="form-control" name="chequera" value="{{ old('chequera') }}" id="checkbook">
                        @foreach($checkbooks as $checkbook)
                        <option value="{{ $checkbook->id }}">{{ $checkbook->name }}</option>
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group">
                      <label for="supplier">Acreedor</label>
                      <select class="form-control" name="proveedor" value="{{ old('proveedor') }}" id="supplier">
                        @foreach($suppliers as $supplier)
                        <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                        @endforeach
                      </select>
                    </div>
                    
                    
                  <div class="box-footer">
                    <!-- <button type="submit" class="btn btn-default">Cancelar</button> -->
                    <button type="submit" class="btn btn-primary pull-right">Agregar</button>
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('scripts')
<!-- InputMask -->
<script src="/plugins/input-mask/jquery.inputmask.js"></script>
<script src="/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
  //Money Euro
    // $('[data-mask]').inputmask();
    $('.select2').select2();

    $('form#addNewCheck').submit(function(){
      $(this).find(':input[type=submit]').prop('disabled', true);
    });
</script>
@endsection