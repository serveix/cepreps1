@extends('layouts.printable')
@section('content')
<div class="container-fluid" style="font-family: Calibri, Arial, sans-serif;">
    <div class="row" style="margin-top:60px">
        <div class="col-xs-4 col-xs-offset-4">
            {{ strtoupper(strftime ('%d de %B de %Y', strtotime($check->created_at) )) }}
        </div>
    </div>
    <div class="row" style="margin-top:25px">
        <div class="col-xs-6 col-xs-offset-2">{{$check->supplier->name}}</div>
        <div class="col-xs-2">$ {{ $check->amount }}</div>
    </div>
    <div class="row" style="margin-top:25px">
        <div class="col-xs-8 col-xs-offset-2"><span id="numberInLetter"></span></div>
    </div>
    <div class="row" style="margin-top:100px">
        <div class="col-xs-8 col-xs-offset-2">
            {{ $check->description }}
            @if(!empty($check->invoice_number))
                <br>
                FACTURA NO. {{$check->invoice_number}}
            @endif
        </div>
    </div>
    <div class="row" style="margin-top:200px">
        <div class="col-xs-3 col-xs-offset-1">ata</div>
        <div class="col-xs-4">DR RML</div>
        <div class="col-xs-2">CHB</div>
        <div class="col-xs-1">{{ $check->number }}</div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('dist/js/numberToLetter.js') }}"></script>

<script>
window.print()
const numberToConvert = {{ $check->amount }}
$('#numberInLetter').html( NumeroALetras(numberToConvert) )
</script>


@endsection