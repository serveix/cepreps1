@extends('layouts.printable')
@section('content')
    <div class="container" style="font-family: Calibri, Arial, sans-serif; padding: 10%;">
        <div class="row">
            <div class="col text-center"><h1 style="font-weight: normal; font-size: 16pt;">RECIBO DE COBRO</h1></div>
        </div>
        <br>
        <div class="row">
            <div class="col text-right">
{{--                {{ date('d F Y', strtotime($check->created_at)) }}--}}
                {{ strtoupper(strftime ('%A %d de %B de %Y', strtotime($check->created_at) )) }}
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-3">
                RECIBI DE:
            </div>
            <div class="col-xs-9" style="border-bottom: solid 1px black;">
                SERVICIO DE CEPREP DEL HOSPITAL UNIVERSITARIO "Dr. Jos&eacute; E. Gonzalez"
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3">
                LA CANTIDAD DE:
            </div>
            <div class="col-xs-9" style="border-bottom: solid 1px black;">
                $ &emsp; {{ $check->amount }} &thinsp; <span id="numberInLetter"></span>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3">
                POR CONCEPTO DE:
            </div>
            <div class="col-xs-9" style="border-bottom: solid 1px black;">
                {{ $check->description }}
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4 col-xs-offset-8" >
                <b>Cta. &thinsp; 0569016481 &emsp; CH</b>
            </div>
        </div>


        <br><br>
        <div class="row">
            <div class="col text-center">
                <b>
                    __________________________________________ <br>
                    ALEJANDRA CAMACHO HERNANDEZ<br>
                    FIRMA Y FECHA DE RECIBIDO
                </b>
            </div>
        </div>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('dist/js/numberToLetter.js') }}"></script>

    <script>
        window.print()
        const numberToConvert = {{ $check->amount }}
        $('#numberInLetter').html( NumeroALetras(numberToConvert) )
    </script>


@endsection