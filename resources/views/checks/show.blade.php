@extends('layouts.default')

@section('content')    

@section('page-header', 'Cheque')
@section('page-header-small', 'INFORMACIÓN')

<a class="btn btn-primary" href="{{ route('indexChecks', ['checkbook' => $check->checkbook->id ]) }}">
    <i class="fas fa-arrow-left"></i> Regresar
</a>

<div class="content">
    @if(session('success'))
    <div class="alert alert-success">
        Se elimino el usuario.
    </div>
    @endif
    
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Cheque #{{$check->number}}</h3>
        </div>
        <div class="box-body">
            <strong>Fecha de creaci&oacute;n:</strong> {{ $check->created_at }} <br>
            <strong>Proveedor:</strong> {{ $check->supplier->name }}<br>
            <strong>Cantidad:</strong> {{ $check->amount }}<br>
            <strong>Concepto:</strong> {{ $check->description }}<br>
            <strong>N&uacute;mero de factura: </strong> {{ $check->invoice_number }}<br>
            <hr>
            <a id="printBtn" target="_blank" class="btn btn-primary" >Imprimir poliza <i class="fas fa-print"></i></a>
            <iframe id="printableFrame" style="display:none" src="" frameborder="0"></iframe>

            <a id="printTicketBtn" target="_blank" class="btn btn-primary" >Imprimir comprobante <i class="fas fa-print"></i></a>
            <iframe id="printableTicketFrame" style="display:none" src="" frameborder="0"></iframe>
        </div>
    </div>
</div>

@endsection


@section('scripts')
<script>
   $(document).ready(() => {
    $('#suppliers').DataTable();
    $('#printBtn').click(e => {
        $('#printableFrame').attr('src', '{{ route('showPrintableCheck', ['check' => $check->id, 'type' => 'poliza']) }}');
    });
    $('#printTicketBtn').click(e => {
        $('#printableTicketFrame').attr('src', '{{ route('showPrintableCheck', ['check' => $check->id, 'type' => 'comprobante']) }}');
    })
} );   
</script>
@endsection