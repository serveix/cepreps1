@extends('layouts.default')

@section('content')    

@section('page-header', 'Cheques')
@section('page-header-small', 'ADMINISTRACIÓN')

<div class="content">
    @if(session('success'))
    <div class="alert alert-success">
        Se elimino el cheque.
    </div>
    @endif
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Lista de Cheques</h3>
        </div>
        <div class="box-body">
            <p id="date_filter">
                <label for="checkbook">Chequera:</label>
                <form method="GET">
                    <select name="checkbook" id="checkbook">
                        @foreach($checkbooks as $checkbook)
                        <option value="{{ $checkbook->id }}"
                            @if($selectedCheckbook == $checkbook->id) selected @endif >
                            {{ $checkbook->name }}
                        </option>
                        @endforeach
                    </select>
                    <br><br>

                    <span id="date-label-from" class="date-label">Fecha inicio: </span>
                    <input name="date-from" class="date_range_filter date" type="date" id="datepicker-from" value="{{ app('request')->input('date-from') }}"/>
                    
                    <span id="date-label-to" class="date-label">Fecha fin:
                    <input name="date-to" class="date_range_filter date" type="date" id="datepicker-to" value="{{ app('request')->input('date-to') }}"/>

                    <input type="submit" value="Filtrar fechas" class="btn btn-primary">
                </form>
            </p>
            <table id="checks" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>No. Cheque</th>
                        <th>Proveedor</th>
                        <th>Concepto</th>
                        <th>No. de Factura</th>
                        <th>Cantidad</th>
                        <th>Chequera</th>
                        <th>Fecha</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($checks as $check)
                    <tr>
                        <td>{{ $check->number }}</td>
                        <td>{{ $check->supplier->name }}</td>
                        <td>{{ $check->description }}</td>
                        <td>{{ $check->invoice_number }}</td>
                        <td>{{ $check->amount }}</td>
                        <td>{{ $check->checkbook->name }}</td>
                        <td>{{ $check->created_at->format('d M Y') }}</td>
                        <td>
                            <a class="btn btn-link" href="{{ route('showCheck', ['check' => $check->id]) }}">
                                <i class="fas fa-eye"></i> Ver
                            </a>
                            <form method="POST" action="{{route('destroyCheck', ['check' => $check->id])}}">
                                {{csrf_field()}}
                                <button type="submit" onclick="return confirm('Seguro desea eliminar cheque?');" class="btn btn-link">
                                    <i class="fas fa-trash-alt"></i> Eliminar
                                </button>
                            </form>
                        </td>
                    </tr>    
                    @endforeach             
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection


@section('scripts')
<script>
   $(document).ready(function() {
    $( "#checkbook" ).change(function() {
        $(location).attr('href','?checkbook=' + this.value);
    });

    $('#checks').DataTable({
        dom: 'Bfrtip',
        "oLanguage": {
            "sSearch": "Buscar:"
        },
        buttons: [
            {
                extend: 'excelHtml5',
                title: 'Relación de Cheques',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4 ]
                }
            },
            {
                extend: 'pdfHtml5',
                title: 'Relación de Cheques',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4 ]
                }
            },
            {
                extend: 'print',
                title: 'Relación de Cheques',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4 ]
                }
            },
        ]
    });


});   
</script>
@endsection