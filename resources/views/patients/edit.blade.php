@extends('layouts.default') @section('page-header', 'Pacientes') @section('page-header-small', 'AGREGAR
NUEVO') @section('content')

<div class="content">
  <div class="row">
      <div class="col-md-12">
          <form action="{{route('findAndShowPatient')}}" method="POST">
              {{ csrf_field() }}
              <input name="searchedUserId" type="hidden" value="{{ $patient->id  }}"/>
              <button class="btn btn-primary" type="submit">Regresar</button>
          </form>
      </div>
  </div>
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Datos del paciente: {{$patient->name}}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        <div class="box-body">
          @if ($errors->editPatient->any())
          <div class="alert alert-danger">
            Se producieron los siguientes errores al intentar actualizar al paciente:
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif @if (session('editSuccess'))
          <div class="alert alert-success">
            ¡El paciente se actualizo con éxito!
          </div>
          @endif

          <form action="{{ route('updatePatient') }}" method="POST">

            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ $patient->id }}" id="p-id">
            <div class="form-group">
              <label for="p-name">Nombre completo</label>
              <input type="text" name="nombre" value="{{ $patient->name }}" class="form-control" id="p-name" placeholder="Juan Isai">
            </div>

            <div class="form-group row">
              <div class="col-md-3">
                <label for="p-homephone">Teléfono de casa</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" id="p-homephone" value="{{ $patient->home_phone }}" name="tel_casa" class="form-control" data-inputmask='"mask": "(99) 9999-9999"'
                    data-mask placeholder="(99) 9999-9999">
                </div>
              </div>

              <div class="col-md-3">
                <label for="p-officephone">Teléfono de oficina</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" id="p-officephone" value="{{ $patient->office_phone }}" name="tel_oficina" class="form-control" data-inputmask='"mask": "(99) 9999-9999"'
                    data-mask placeholder="(99) 9999-9999">
                </div>
              </div>

              <div class="col-md-3">
                <label for="p-celphone">Teléfono celular</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-mobile"></i>
                  </div>
                  <input type="text" id="p-celphone" name="celular" value="{{ $patient->celphone }}" class="form-control" data-inputmask='"mask": "(99) 9999-9999"'
                    data-mask placeholder="(999) 999-999-9999">
                </div>
              </div>


              <div class="col-md-3">
                <label for="p-email">Correo electrónico</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-envelope"></i>
                  </div>

                  <input type="text" class="form-control" name="email" id="p-email" value="{{ $patient->email }}" placeholder="john.doe@example.com">

                </div>
              </div>

            </div>

            <div class="form-group row">
              <div class="col-md-4">
                <label for="p-birthdate">Fecha de nacimiento</label>

                <input type="date" id="p-birthdate" name="fecha_nacimiento" value="{{ $patient->birthdate }}" class="form-control" placeholder="DD/MM/AA">
              </div>

              <div class="col-md-4">
                <label for="p-birthplace">Lugar de nacimiento</label>

                <input type="text" id="p-birthplace" name="lugar_nacimiento" value="{{ $patient->birthplace }}" class="form-control" placeholder="Monterrey">
              </div>

              <div class="col-md-4">
                <label for="p-referral">Referenciado por</label>
                <input type="text" class="form-control" id="p-referral" name="referido" value="{{ $patient->referral }}" placeholder="Silvia Tellez">
              </div>
            </div>

            <div class="form-group row">

              <div class="col-md-3">
                <label>Calle y número </label>
                <input type="text" class="form-control" id="p-address1" name="calle_numero" value="{{ $patient->address1 }}" placeholder="Zafiro 120">
              </div>
              <div class="col-md-3">
                <label>Colonia</label>
                <input type="text" class="form-control" id="p-address2" name="colonia" value="{{ $patient->address2 }}" placeholder="Las Puentes">
              </div>

              <div class="col-md-3">
                <label>Indicaciones</label>
                <input type="text" class="form-control" id="p-indications" name="indicaciones" value="{{ $patient->indications }}" placeholder="Entre calles azufre y andador">
              </div>

              <div class="col-md-3">
                <label>Código postal</label>
                <input type="text" class="form-control" id="p-zipcode" name="codigo_postal" value="{{ $patient->zip_code }}" placeholder="66330">
              </div>

            </div>

            <div class="form-group row">

              <div class="col-md-6">
                <label>Ciudad</label>
                <input type="text" class="form-control" id="p-city" name="ciudad" value="{{ $patient->city }}" placeholder="Monterrey">
              </div>
              <div class="col-md-6">
                <label>Estado</label>
                <select id="p-state" name="estado" value="{{ $patient->state }}" class="form-control select2">
                  <option value="Aguascalientes" {{ $patient->state == 'Aguascalientes' ? 'selected' : '' }}>Aguascalientes</option>
                  <option value="Baja California" {{ $patient->state == 'Baja California' ? 'selected' : '' }}>Baja California </option>
                  <option value="Baja California Sur" {{ $patient->state == 'Baja California Sur' ? 'selected' : '' }}>Baja California Sur </option>
                  <option value="Campeche" {{ $patient->state == 'Campeche' ? 'selected' : '' }}>Campeche </option>
                  <option value="Chiapas" {{ $patient->state == 'Chiapas' ? 'selected' : '' }}>Chiapas </option>
                  <option value="Chihuahua" {{ $patient->state == 'Chihuahua' ? 'selected' : '' }}>Chihuahua </option>
                  <option value="Coahuila" {{ $patient->state == 'Coahuila' ? 'selected' : '' }}>Coahuila </option>
                  <option value="Colima" {{ $patient->state == 'Colima' ? 'selected' : '' }}>Colima </option>
                  <option value="Distrito Federal" {{ $patient->state == 'Distrito Federal' ? 'selected' : '' }}>Distrito Federal</option>
                  <option value="Durango" {{ $patient->state == 'Durango' ? 'selected' : '' }}>Durango </option>
                  <option value="Estado de México" {{ $patient->state == 'Estado de México' ? 'selected' : '' }}>Estado de México </option>
                  <option value="Guanajuato" {{ $patient->state == 'Guanajuato' ? 'selected' : '' }}>Guanajuato </option>
                  <option value="Guerrero" {{ $patient->state == 'Guerrero' ? 'selected' : '' }}>Guerrero </option>
                  <option value="Hidalgo" {{ $patient->state == 'Hidalgo' ? 'selected' : '' }}>Hidalgo </option>
                  <option value="Jalisco" {{ $patient->state == 'Jalisco' ? 'selected' : '' }}>Jalisco </option>
                  <option value="Michoacán" {{ $patient->state == 'Michoacán' ? 'selected' : '' }}>Michoacán </option>
                  <option value="Morelos" {{ $patient->state == 'Morelos' ? 'selected' : '' }}>Morelos </option>
                  <option value="Nayarit" {{ $patient->state == 'Nayarit' ? 'selected' : '' }}>Nayarit </option>
                  <option value="Nuevo León" {{ $patient->state == 'Nuevo León' ? 'selected' : '' }}>Nuevo León </option>
                  <option value="Oaxaca" {{ $patient->state == 'Oaxaca' ? 'selected' : '' }}>Oaxaca </option>
                  <option value="Puebla" {{ $patient->state == 'Puebla' ? 'selected' : '' }}>Puebla </option>
                  <option value="Querétaro" {{ $patient->state == 'Querétaro' ? 'selected' : '' }}>Querétaro </option>
                  <option value="Quintana Roo" {{ $patient->state == 'Quintana Roo' ? 'selected' : '' }}>Quintana Roo </option>
                  <option value="San Luis Potosí" {{ $patient->state == 'San Luis Potosí' ? 'selected' : '' }}>San Luis Potosí </option>
                  <option value="Sinaloa" {{ $patient->state == 'Sinaloa' ? 'selected' : '' }}>Sinaloa </option>
                  <option value="Sonora" {{ $patient->state == 'Sonora' ? 'selected' : '' }}>Sonora </option>
                  <option value="Tabasco" {{ $patient->state == 'Tabasco' ? 'selected' : '' }}>Tabasco </option>
                  <option value="Tamaulipas" {{ $patient->state == 'Tamaulipas' ? 'selected' : '' }}>Tamaulipas </option>
                  <option value="Tlaxcala" {{ $patient->state == 'Tlaxcala' ? 'selected' : '' }}>Tlaxcala </option>
                  <option value="Veracruz" {{ $patient->state == 'Veracruz' ? 'selected' : '' }}>Veracruz </option>
                  <option value="Yucatán" {{ $patient->state == 'Yucatán' ? 'selected' : '' }}>Yucatán </option>
                  <option value="Zacatecas" {{ $patient->state == 'Zacatecas' ? 'selected' : '' }}>Zacatecas</option>
                </select>
              </div>

            </div>

            <div class="form-group">
              <label>Informaci&oacute;n adicional</label>
              <textarea class="form-control" name="informacion_adicional" id="p-additionalinfo" placeholder="Detalles extra sobre el paciente">{{ $patient->additional_info }}</textarea>
            </div>





            <div class="box-footer">
              <button type="submit" class="btn btn-default">Cancelar</button>
              <button type="submit" class="btn btn-primary pull-right">Agregar</button>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection @section('scripts')
<!-- InputMask -->
<script src="/plugins/input-mask/jquery.inputmask.js"></script>
<script src="/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
  //Money Euro
  $('[data-mask]').inputmask();
  $('.select2').select2();
</script> @endsection