@extends('layouts.default')

@section('content')    
@section('page-header', 'Pacientes')
@section('page-header-small')
DETALLES DE PACIENTE 
<div class="container">
    <div class="row">
        <div class="col-md-1">
            <form action="{{route('findAndShowPatient')}}" method="POST">
                {{ csrf_field() }}
                <input name="searchedUserId" type="hidden" value="{{ $patient->id  }}"/>
                <button class="btn btn-primary" type="submit">Regresar</button>
            </form>
        </div>
        <div class="col-xs-1 col-xs-offset-9">
            <a id="toPrint" target="_blank" class="btn btn-primary" >Imprimir <i class="fas fa-print"></i></a>
        </div>
    </div>
</div>
@endsection

<div class="content container-fluid">
    <div class="row">
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-3">
                    <img src="/dist/img/logoUANL.jpg" width="100%">
                </div>
                <div class="col-xs-9">
                <strong>UNIVERSIDAD AUTONOMA DE NUEVO LEON</strong><br>
                HOSPITAL UNIVERSITARIO "Dr. Jose Eleuterio Gonzalez"<br>
                C.E.P.R.E.P.<br>
                FICHA DE IDENTIFICACI&Oacute;N
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <img src="/dist/img/logoCEPREP.jpg" height="100px" align="right">
        </div>
    </div>
    <div class="row top-buffer justify-content-right">
        <div class ="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">REGISTRO DEL PACIENTE #{{ $patient->id }}</h3>
                </div>
                <div class="box-body">

                    <table class="table table-bordered">
                        <thead>
                            <th>IDENTIFICACIÓN</th>
                        </thead>
                        <tbody>
                            <td>
                            <div class="row">
                                <div class="col-xs-12">
                                    <strong>Nombre del paciente:</strong> <br>
                                    {{ $patient->name }}
                                </div>
                            </div>
                            <div class="row top-buffer">
                                <div class="col-xs-4">
                                    <strong>Fecha de nacimiento:</strong> <br>
                                    <p id="fechaNac">{{ $patient->birthdate }}</p>
                                </div>

                                <div class="col-xs-4">
                                    <strong>Edad del paciente:</strong> <br>
                                    <p id="edad">{{$age}}</p>
                                </div>
                                
                                <div class="col-xs-4">
                                    <strong>Lugar de nacimiento:</strong> <br>
                                    {{ $patient->birthplace }}
                                </div>
                            </div>
                            <div class="row top-buffer">
                                <div class="col-xs-6">
                                    <strong>Recomendado por:</strong> <br>
                                    {{ $patient->referral }}
                                </div>

                                <div class="col-xs-6">
                                    <strong>Registrado:</strong> <br>
                                    {{ $patient->created_at }}
                                </div>
                            </div>
                            </td>
                        </tbody>
                    </table>
                    <br>
                     <table class="table table-bordered">
                        <thead>
                            <th>DOMICILIO</th>
                        </thead>
                        <tbody>
                            <td>
                            <div class="row">
                                <div class="col-xs-6 ">
                                    <strong>Calle y número:</strong> <br>
                                    {{ $patient->address1 }}
                                </div>
                                <div class="col-xs-6">
                                    <strong>Entre calles:</strong>  <br>
                                    {{ $patient->indications }}
                                </div>
                            </div>
                            <div class="row top-buffer">
                                <div class="col-xs-3">
                                    <strong>Colonia:</strong><br>
                                    {{ $patient->address2 }}
                                </div>

                                <div class="col-xs-3">
                                    <strong>Ciudad:</strong><br>
                                    {{ $patient->city }}
                                </div>
                                
                                <div class="col-xs-3">
                                    <strong>Estado:</strong><br>
                                     {{ $patient->state }}
                                </div>

                                <div class="col-xs-3">
                                    <strong>Código Postal:</strong><br>
                                     {{ $patient->zip_code }}
                                </div>
                            </div>
                            </td>
                        </tbody>
                    </table>
                    <br>
                    <table class="table table-bordered">
                        <thead>
                            <th>TELEFONOS</th>
                        </thead>
                        <tbody>
                            <td>
                            <div class="row">
                                <div class="col-xs-4 ">
                                    <strong>Casa:</strong> <br>
                                    {{ $patient->home_phone }}
                                </div>

                                <div class="col-xs-4">
                                    <strong>Oficina:</strong>  <br>
                                    {{ $patient->office_phone }}
                                </div>

                                <div class="col-xs-4">
                                    <strong>Celular:</strong>  <br>
                                    {{ $patient->celphone }}
                                </div>
                            </div>
                            </td>
                        </tbody>
                    </table>
                    <br>
                    <table class="table table-bordered">
                        <thead>
                            <th>COMENTARIOS</th>
                        </thead>
                        <tbody>
                            <td>
                            <div class="row">
                                <div class="col-xs-12">
                                    {{ $patient->additional_info }}
                                </div>
                            </div>
                            </td>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('scripts')
<script type="text/javascript">
    $('#toPrint').click(function() {
        window.print();
    });
</script>
@endsection