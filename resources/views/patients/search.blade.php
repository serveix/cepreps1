@extends('layouts.default')

@section('page-header', 'Buscar Paciente')

@section('page-header-small', 'PACIENTE')

@section('breadcrumb')
  <li><a href="#"><i class="fa fa-dashboard"></i>Buscar un Paciente</a></li>
  <li class="active">Buscar un Paciente</li>
@endsection

@section('content')    

<div class="col-md-8 col-md-offset-2">
	<div class="box box-primary">
		<div class="box-header with-border">
		    <h3 class="box-title">Empleado: {{ Auth::user()->name }}</h3>
		</div>

	<!-- /.box-header -->
		<div class="box-body">
	        <div id="searchform" class="form-group">
				<h4>Paciente</h4>

				<div id="searchpatient" class="">
					<label>Escribe el nombre, apellido o número de paciente para buscar el paciente deseado</label>
					<form id="searchForm">
						<div class="form-group">
							<div class="input-group">
								<input autocomplete="off" type="text" class="form-control" id="pacientsearch"/>
								<div class="input-group-btn">
									<button type="submit" class="btn btn-primary" id="btnsearch"><span aria-hidden="true" class="glyphicon glyphicon-search"></span></button>
								</div>
							</div>
						</div>
					</form>
				    
				</div>
                <div id="showResults" class="hidden">
					<center><img src="/dist/img/loading.gif"></center>
                </div>
            </div>
		
		 <!-- /.row --> 
		</div>
	<!-- /.box-body -->   
	</div>
	<!-- /.box -->

</div>
<!-- /.col -->
@endsection
@section('scripts')
<script src="/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
    var firstTime = true;
	function getSearchPatients(search){
	   return $.ajax({
	        url: "/patients/search/" + search + "/" + 8,
		    type: "get"
	   });
	}

    function putPatientSearch(showResultsDiv) {
		var search = $("#pacientsearch").val();
		if(search == "")
		    search = '{{ $foundPatient->id or "" }}';

	   	getSearchPatients(search).done(function(view){
			showResultsDiv.removeClass('hidden');
			showResultsDiv.html(view);

			@if(!empty($foundPatient))
			if(firstTime){
				$('#modal-actions-{{ $foundPatient->id }}').modal('toggle');
				firstTime = false;
		    }
			@endif
		});
	}

   //JQuery
   $(document).ready(function(){
	   //Sucede cuando le dan clic al boton de buscar un paciente.
	   var showResultsDiv = $('#showResults');  
	   
	   $('form').on('submit', function(e){
		    e.preventDefault();
		    putPatientSearch( showResultsDiv );
	   });

	   @if(!empty($foundPatient))
	   putPatientSearch( showResultsDiv );
	   console.log(1231232);
	   @endif
   });

</script>

@endsection