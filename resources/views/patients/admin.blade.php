@extends('layouts.default')

@section('content')    
 
@section('page-header', 'Pacientes')
@section('page-header-small', 'ADMINISTRACIÓN')

<div class="content">
    <div class="row justify-content-center">
        <div class="col-md-12">
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Acci&oacute;nes</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($patients as $patient)
                        <tr>
                            <td>{{ $patient->id }}</td>
                            <td><a href="{{ route('patientDetails', ['id' => $patient->id]) }}">{{ $patient->name }}</a></td>
                            <td>{{ $patient->email or 'Sin email' }}</td>
                            <td>
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <a href="#">
                                            <i class="fas fa-info-circle"></i> Ver
                                        </a>
                                    </li>
                                    
                                    <li class="list-inline-item">
                                        <a href="#">
                                            <i class="fas fa-edit"></i> Editar
                                        </a>
                                    </li>

                                    <li class="list-inline-item">
                                        <a href="#">
                                            <i class="fas fa-trash-alt"></i> Eliminar
                                        </a>
                                    </li>
                                </ul>
                            </td>
                        </tr>    
                        @endforeach             
                    </tbody>
                </table>
        </div>
        
    </div>
</div>

@endsection


@section('scripts')
<script>
   $(document).ready(function() {
    $('#example').DataTable();
} );   
</script>
@endsection