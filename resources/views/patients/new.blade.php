@extends('layouts.default')

@section('content')    
@section('page-header', 'Pacientes')
@section('page-header-small', 'AGREGAR NUEVO')

<div class="content">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Datos del paciente</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                
                  <div class="box-body">
                  @if ($errors->any())
                      <div class="alert alert-danger">
                          Se producieron los siguientes errores al intentar agregar al paciente:
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif

                  @if (session('status'))
                      <div class="alert alert-success">
                          {{ session('status') }}
                      </div>
                  @endif

                  <form id="addNewPatient" action="{{ route('addNewPatient') }}" method="POST">
                    
                    {{ csrf_field() }}

                    <div class="form-group">
                      <label for="p-name">Nombre completo</label>
                      <input type="text" name="nombre" value="{{ old('nombre') }}" class="form-control" id="p-name" placeholder="Juan Isai">    
                    </div>

                    <div class="form-group row">
                      <div class="col-md-3">
                        <label for="p-homephone">Teléfono de casa</label>

                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                          </div>
                          <input type="text" id="p-homephone" value="{{ old('tel_casa') }}" name="tel_casa" class="form-control" >
                        </div>
                      </div>

                      <div class="col-md-3">
                        <label for="p-officephone">Teléfono de oficina</label>

                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                          </div>
                          <input type="text" id="p-officephone" value="{{ old('tel_oficina') }}" name="tel_oficina" class="form-control" >
                        </div>
                      </div>

                      <div class="col-md-3">
                        <label for="p-celphone">Teléfono celular</label>

                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-mobile"></i>
                          </div>
                          <input type="text" id="p-celphone" name="celular" value="{{ old('celular') }}" class="form-control" >
                        </div>
                      </div>
                      

                      <div class="col-md-3">
                        <label for="p-email">Correo electrónico</label>

                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-envelope"></i>
                          </div>
                          
                          <input type="text" class="form-control" name="email" id="p-email" value="{{ old('email') }}" placeholder="john.doe@example.com">
                          
                        </div>
                      </div>

                    </div>

                    <div class="form-group row">
                      <div class="col-md-4">
                        <label for="p-birthdate">Fecha de nacimiento</label>

                        <input type="date" id="p-birthdate" name="fecha_nacimiento" value="{{ old('fecha_nacimiento') }}" class="form-control" placeholder="DD/MM/AA">
                      </div>

                      <div class="col-md-4">
                        <label for="p-birthplace">Lugar de nacimiento</label>

                        <input type="text" id="p-birthplace" name="lugar_nacimiento" value="{{ old('lugar_nacimiento') }}" class="form-control" placeholder="Monterrey">
                      </div>

                      <div class="col-md-4">
                        <label for="p-referral">Referenciado por</label>
                        <input type="text" class="form-control" id="p-referral" name="referido" value="{{ old('referido') }}" placeholder="Hospital">
                      </div>
                    </div>

                    <div class="form-group row">
                      
                        <div class="col-md-3">
                          <label>Calle y número </label>
                          <input type="text" class="form-control" id="p-address1" name="calle_numero" value="{{ old('calle_numero') }}"  placeholder="Zafiro 120">
                        </div>
                        <div class="col-md-3">
                          <label>Colonia</label>
                          <input type="text" class="form-control" id="p-address2" name="colonia" value="{{ old('colonia') }}" placeholder="Las Puentes">
                        </div>

                        <div class="col-md-3">
                          <label>Indicaciones</label>
                          <input type="text" class="form-control" id="p-indications" name="indicaciones" value="{{ old('indicaciones') }}" placeholder="Entre calles azufre y andador">
                        </div>

                        <div class="col-md-3">
                          <label>Código postal</label>
                          <input type="text" class="form-control" id="p-zipcode" name="codigo_postal" value="{{ old('codigo_postal') }}" placeholder="66330">
                        </div>
                      
                    </div>

                    <div class="form-group row">
                      
                        <div class="col-md-6">
                          <label>Ciudad</label>
                          <input type="text" class="form-control" id="p-city" name="ciudad" value="{{ old('ciudad') }}" placeholder="Monterrey">
                        </div>
                        <div class="col-md-6">
                          <label>Estado</label>
                          <select id="p-state" name="estado" value="{{ old('estado') }}" class="form-control select2">
                            <option value="Aguascalientes" {{ old('estado') == 'Aguascalientes' ? 'selected' : '' }}>Aguascalientes</option>
                            <option value="Baja California" {{ old('estado') == 'Baja California' ? 'selected' : '' }}>Baja California </option>
                            <option value="Baja California Sur" {{ old('estado') == 'Baja California Sur' ? 'selected' : '' }}>Baja California Sur </option>
                            <option value="Campeche" {{ old('estado') == 'Campeche' ? 'selected' : '' }}>Campeche </option>
                            <option value="Chiapas" {{ old('estado') == 'Chiapas' ? 'selected' : '' }}>Chiapas </option>
                            <option value="Chihuahua" {{ old('estado') == 'Chihuahua' ? 'selected' : '' }}>Chihuahua </option>
                            <option value="Coahuila" {{ old('estado') == 'Coahuila' ? 'selected' : '' }}>Coahuila </option>
                            <option value="Colima" {{ old('estado') == 'Colima' ? 'selected' : '' }}>Colima </option>
                            <option value="Distrito Federal" {{ old('estado') == 'Distrito Federal' ? 'selected' : '' }}>Distrito Federal</option>
                            <option value="Durango" {{ old('estado') == 'Durango' ? 'selected' : '' }}>Durango </option>
                            <option value="Estado de México" {{ old('estado') == 'Estado de México' ? 'selected' : '' }}>Estado de México </option>
                            <option value="Guanajuato" {{ old('estado') == 'Guanajuato' ? 'selected' : '' }}>Guanajuato </option>
                            <option value="Guerrero" {{ old('estado') == 'Guerrero' ? 'selected' : '' }}>Guerrero </option>
                            <option value="Hidalgo" {{ old('estado') == 'Hidalgo' ? 'selected' : '' }}>Hidalgo </option>
                            <option value="Jalisco" {{ old('estado') == 'Jalisco' ? 'selected' : '' }}>Jalisco </option>
                            <option value="Michoacán" {{ old('estado') == 'Michoacán' ? 'selected' : '' }}>Michoacán </option>
                            <option value="Morelos" {{ old('estado') == 'Morelos' ? 'selected' : '' }}>Morelos </option>
                            <option value="Nayarit" {{ old('estado') == 'Nayarit' ? 'selected' : '' }}>Nayarit </option>
                            <option value="Nuevo León" {{ old('estado') == 'Nuevo León' ? 'selected' : '' }}>Nuevo León </option>
                            <option value="Oaxaca" {{ old('estado') == 'Oaxaca' ? 'selected' : '' }}>Oaxaca </option>
                            <option value="Puebla" {{ old('estado') == 'Puebla' ? 'selected' : '' }}>Puebla </option>
                            <option value="Querétaro" {{ old('estado') == 'Querétaro' ? 'selected' : '' }}>Querétaro </option>
                            <option value="Quintana Roo" {{ old('estado') == 'Quintana Roo' ? 'selected' : '' }}>Quintana Roo </option>
                            <option value="San Luis Potosí" {{ old('estado') == 'San Luis Potosí' ? 'selected' : '' }}>San Luis Potosí </option>
                            <option value="Sinaloa" {{ old('estado') == 'Sinaloa' ? 'selected' : '' }}>Sinaloa </option>
                            <option value="Sonora" {{ old('estado') == 'Sonora' ? 'selected' : '' }}>Sonora </option>
                            <option value="Tabasco" {{ old('estado') == 'Tabasco' ? 'selected' : '' }}>Tabasco </option>
                            <option value="Tamaulipas" {{ old('estado') == 'Tamaulipas' ? 'selected' : '' }}>Tamaulipas </option>
                            <option value="Tlaxcala" {{ old('estado') == 'Tlaxcala' ? 'selected' : '' }}>Tlaxcala </option>
                            <option value="Veracruz" {{ old('estado') == 'Veracruz' ? 'selected' : '' }}>Veracruz </option>
                            <option value="Yucatán" {{ old('estado') == 'Yucatán' ? 'selected' : '' }}>Yucatán </option>
                            <option value="Zacatecas" {{ old('estado') == 'Zacatecas' ? 'selected' : '' }}>Zacatecas</option>
                          </select>
                        </div>
                      
                    </div>

                    <div class="form-group">
                      <label>Informaci&oacute;n adicional</label>
                      <textarea class="form-control" name="informacion_adicional" id="p-additionalinfo" placeholder="Detalles extra sobre el paciente">{{ old('informacion_adicional') }}</textarea>
                    </div>
                    

                    

                    
                  <div class="box-footer">
                    <button type="submit" class="btn btn-default">Cancelar</button>
                    <button type="submit" class="btn btn-primary pull-right">Agregar</button>
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('scripts')
<!-- InputMask -->
<script src="/plugins/input-mask/jquery.inputmask.js"></script>
<script src="/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
  //Money Euro
    // $('[data-mask]').inputmask();
    $('.select2').select2();

    $('form#addNewPatient').submit(function(){
      $(this).find(':input[type=submit]').prop('disabled', true);
    });
</script>
@endsection