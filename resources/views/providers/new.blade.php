@extends('layouts.default')

@section('content')    
@section('page-header', 'Pacientes')
@section('page-header-small', 'AGREGAR NUEVO')

<div class="content">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Datos del proveedor</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                
                  <div class="box-body">
                  @if ($errors->any())
                      <div class="alert alert-danger">
                          Se producieron los siguientes errores al intentar agregar al paciente:
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif

                  @if (session('success'))
                      <div class="alert alert-success">
                          Se agreg&oacute; el proveedor con &eacute;xito.
                      </div>
                  @endif

                  <form action="{{ route('addNewProvider') }}" method="POST">
                    
                    {{ csrf_field() }}

                    <div class="form-group">
                      <label for="p-name">Nombre </label>
                      <input type="text" name="nombre" maxlength="191" value="{{ old('nombre') }}" class="form-control" id="p-name" placeholder="Ej. Universidad Autonoma de Nuevo Le&oacute;n">    
                    </div>

                    <div class="form-group">
                        <label for="p-rfc">RFC </label>
                        <input type="text" name="rfc" maxlength="191" value="{{ old('rfc') }}" class="form-control" id="p-rfc" placeholder="Ej. AAAA000000XXX">    
                    </div>

                    <div class="form-group">
                        <label for="p-addr">Domicilio </label>
                        <input type="text" name="domicilio" maxlength="191" value="{{ old('domicilio') }}" class="form-control" id="p-addr" placeholder="Ej. Calle Doe #999">    
                    </div>

                    <div class="form-group">
                        <label for="p-tel">Tel&eacute;fono </label>
                        <input type="text" name="telefono" maxlength="191" value="{{ old('telefono') }}" class="form-control" id="p-tel" placeholder="Ej. 8119338822">    
                    </div>

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">Agregar</button>
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('scripts')

@endsection