
@extends('layouts.default')

@section('content')    

@section('page-header', 'Proveedor')
@section('page-header-small', 'Recibos')

<div class="container">
    <div class="row">
        <div class="col-md-1">
            <a href="/providers/find" class="btn btn-primary">Regresar a Proveedores</a>
        </div>
    </div>
</div>

<div class="content">
    @if(session('success'))
        <div class="alert alert-success">
            Se ha eliminado el recibo con &eacute;xito.
        </div>
    @endif
    <div class="box box-primary">
        <div class="box-header">
            <h2 >PROVEEDOR {{$provider->name}} </h2>
        </div>
        <div class="box-body">
            
            <table id="providers" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th># FOLIO DE RECIBO</th>
                        <th>Nombre Proveedor</th>
                        <th>Nombre RFC</th>
                        <th>Fecha del Recibo</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($provider->servicesCharges as $sc)
                    <tr>
                        <td>{{ $sc->id }}</td>
                        <td>{{ $sc->provider->name OR '...' }}</td>
                        <td>{{ $sc->provider->rfc OR '...' }}</td>
                        <td>{{ $sc->created_at }}</td>
                        <td>

                            <a type="submit" class="btn btn-link" 
                                href="{{ route('viewProviderReceipt', ['charge' => $sc->id]) }}">
                                <i class="fas fa-print"></i> Ver recibo
                            </a>

                            <form method="POST" action="{{route('deleteProviderReceipt', ['charge' => $sc->id])}}">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-link">
                                    <i class="fas fa-trash-alt"></i> Eliminar
                                </button>
                            </form>
                        </td>
                    </tr>    
                    @endforeach             
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection


@section('scripts')
<script>
   $(document).ready(function() {
    $('#providers').DataTable();
} );   
</script>
@endsection