@extends('layouts.default')

@section('content')    

@section('page-header', 'Proveedores')
@section('page-header-small', 'ADMINISTRACIÓN')

<div class="content">
    @if(session('success'))
        <div class="alert alert-success">
            Se ha eliminado el proveedor con &eacute;xito.
        </div>
    @endif
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Proveedores del Sistema CEPREP</h3>
        </div>
        <div class="box-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    Se producieron los siguientes errores al intentar agregar al paciente:
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <table id="providers" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>RFC</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($providers as $provider)
                    <tr>
                        <td>{{ $provider->id }}</td>
                        <td>{{ $provider->name }}</td>
                        <td>{{ $provider->rfc }}</td>
                        <td>
                            {{-- <form method="POST" action="{{route('deleteUser')}}">
                                {{csrf_field()}}
                                <input type="hidden" name="userId" value="{{$user->id}}">
                                <button type="submit" class="btn btn-link">
                                    <i class="fas fa-trash-alt"></i> Eliminar
                                </button>
                            </form> --}}

                            <button type="submit" class="btn btn-link" 
                                data-toggle="modal" data-target="#provider-information-{{$provider->id}}">
                                <i class="fas fa-info-circle"></i> Ver informacion
                            </button>

                            <!-- PROVIDER INFO MODAL -->
                            <div class="modal fade" id="provider-information-{{$provider->id}}">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header" style="background-color:#9b4a89; color:white">
                                            <button type="button" style="color:white" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Proveedor #{{ $provider->id }} </h4>
                                        </div>
                                        
                                        <div class="modal-body">
                                            
                                            <ul>
                                                <li><strong>Nombre: </strong> {{ $provider->name }}</li>
                                                <li><strong>RFC: </strong> {{ $provider->rfc }}</li>
                                                <li><strong>Telefono: </strong> {{ $provider->telefono }}</li>
                                                <li><strong>Domicilio: </strong> {{ $provider->domicilio }}</li>
                                            </ul>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
                                            <!-- <button id="confirm-request" type="submit" class="btn btn-primary">Aceptar</button> -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-link"
                                data-toggle="modal" data-target="#provider-new-{{$provider->id}}">
                                <i class="fas fa-plus"></i> Nuevo recibo
                            </button>

                            <!-- PROVIDER NEW RECIBO MODAL -->
                            <div class="modal fade" id="provider-new-{{$provider->id}}">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header" style="background-color:#9b4a89; color:white">
                                            <button type="button" style="color:white" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Generacion de recibo para proveedor #{{ $provider->id }} </h4>
                                        </div>
                                        <form method="POST" action="{{ route('newProviderCharge', ['provider' => $provider->id]) }}">
                                            {{ csrf_field() }}
                                            <div class="modal-body">
                                                <p class="medium-p text-center">
                                                    Se generara un recibo con fecha de {{ date('d/m/Y') }} y los datos del proveedor
                                                </p>
                                                <p class="text-center">
                                                    <label>Concepto:</label><br>
                                                    <input type="text" maxlength="191" name="concepto" class="form-control">
                                                    <br>
                                                    <label>Cantidad:</label><br>
                                                    <input type="text" name="cantidad" class="form-control">
                                                </p>
                                                    
                                                <p class="medium-p text-center">&iquest;Desea continuar?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                <button id="confirm-request" type="submit" class="btn btn-primary">Continuar</button>
                                            </div>
                                        </form>
                                        
                                    </div>
                                </div>
                            </div>

                            <a type="submit" href="{{ route('viewProviderCharges', ['provider' => $provider->id]) }}" class="btn btn-link">
                                <i class="fas fa-file-alt"></i> Ver recibos
                            </a>

                            <form method="POST" action="{{route('deleteProvider', ['provider' => $provider->id])}}" >
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-link">
                                    <i class="fas fa-trash-alt"></i> Eliminar
                                </button>
                            </form>
                        </td>
                    </tr>    
                    @endforeach             
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection


@section('scripts')
<script>
   $(document).ready(function() {
    $('#providers').DataTable();
} );   
</script>
@endsection