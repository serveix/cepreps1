<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDoctorChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('doctor_charges', function (Blueprint $table) {
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->foreign('doctor_id')
                  ->references('id')->on('doctors')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');

            $table->foreign('patient_id')
                  ->references('id')->on('patients')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');

            $table->foreign('discount_request_id')
                  ->references('id')->on('discount_requests')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('doctor_charges', function (Blueprint $table) {
            //
        });
    }
}
