<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateServicesChargesAddAdditionalInfoColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services_charges', function (Blueprint $table) {
            $table->string('additional_info')->nullable()->after('patient_paid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services_charges', function (Blueprint $table) {
            $table->dropColumn('additional_info');
        });
    }
}
