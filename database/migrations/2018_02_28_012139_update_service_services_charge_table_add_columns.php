<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateServiceServicesChargeTableAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_services_charge', function (Blueprint $table) {
            $table->string('payment_method')->nullable()->after('price');
            $table->string('last_four', 4)->nullable()->after('payment_method');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_services_charge', function (Blueprint $table) {
            $table->dropColumn('payment_method', 'last_four');
        });
    }
}
