<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropDoctorChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('doctor_charges');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->increments('id');
        $table->integer('user_id')->unsigned();
        $table->integer('doctor_id')->unsigned();
        $table->integer('patient_id')->unsigned();
        $table->integer('discount_request_id')->unsigned()->nullable();
        $table->timestamps();
        $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->foreign('doctor_id')
                  ->references('id')->on('doctors')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');

            $table->foreign('patient_id')
                  ->references('id')->on('patients')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');

            $table->foreign('discount_request_id')
                  ->references('id')->on('discount_requests')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
    }
}
