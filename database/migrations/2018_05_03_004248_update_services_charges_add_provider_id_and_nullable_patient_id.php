<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateServicesChargesAddProviderIdAndNullablePatientId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services_charges', function (Blueprint $table) {
            $table->integer('provider_id')->unsigned()->nullable()->after('patient_id');
            $table->foreign('provider_id')->references('id')->on('providers')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->dropForeign('services_charges_patient_id_foreign');
        });

        Schema::table('services_charges', function (Blueprint $table) {
            $table->integer('patient_id')->nullable()->unsigned()->change();
            $table->foreign('patient_id')->references('id')->on('patients')->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services_charges', function (Blueprint $table) {
            $table->dropForeign('services_charges_provider_id_foreign');
            $table->dropColumn('provider_id');
        });

        Schema::table('services_charges', function (Blueprint $table) {
            $table->dropForeign('services_charges_patient_id_foreign');
        });

        Schema::table('services_charges', function (Blueprint $table) {
            $table->integer('patient_id')->unsigned()->nullable(false)->change();
            $table->foreign('patient_id')->references('id')->on('patients')->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }
}
