<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDiscountRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('discount_requests', function (Blueprint $table) {
            $table->foreign('services_charge_id')
                  ->references('id')->on('services_charges')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('services_discount_id')
                  ->references('id')->on('services_discounts')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('discount_requests', function (Blueprint $table) {
            //
        });
    }
}
