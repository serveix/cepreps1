<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateServicesChargesTableAddPatientPaid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services_charges', function (Blueprint $table) {
            $table->decimal('patient_paid', 8, 2)->after('approval_status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services_charges', function (Blueprint $table) {
            $table->dropColumn('patient_paid');
        });
    }
}
