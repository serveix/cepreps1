<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePatientsTable extends Migration
{
    /* Agregamos nullables a las columnas que no son necesarias llenar
    y tambien ahora existe birthplace */

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->string('email')->nullable()->change();
            $table->string('indications')->nullable()->change();
            $table->string('home_phone', 15)->change();
            $table->string('office_phone', 15)->nullable()->change();
            $table->string('celphone', 18)->nullable()->change();
            $table->string('referral')->nullable()->change();
            $table->string('additional_info')->nullable()->change();

            $table->string('birthplace')->after('birthdate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->string('email')->nullable(false)->change();
            $table->string('indications')->nullable(false)->change();
            $table->string('home_phone', 191)->change();
            $table->string('office_phone', 191)->nullable(false)->change();
            $table->string('celphone', 191)->nullable(false)->change();
            $table->string('referral')->nullable(false)->change();
            $table->string('additional_info')->nullable(false)->change();

            $table->dropColumn('birthplace');
        });
    }
}
