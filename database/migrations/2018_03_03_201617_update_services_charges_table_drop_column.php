<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateServicesChargesTableDropColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services_charges', function (Blueprint $table) {
            $table->dropForeign(['discount_request_id']);
            $table->dropColumn('discount_request_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services_charges', function (Blueprint $table) {
            $table->integer('discount_request_id')->unsigned()->after('patient_id');
            $table->foreign('discount_request_id')
                    ->references('id')
                    ->on('discount_requests')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }
}
