<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateServicesChargesAddTotalAmount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services_charges', function (Blueprint $table) {
            $table->decimal('total_amount', 8, 2)->after('discount_request_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services_charges', function (Blueprint $table) {
            $table->dropColumn('total_amount');
        });
    }
}
