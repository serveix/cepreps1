<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateServiceServicesChargeRenameServiceChargeIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_services_charge', function (Blueprint $table) {
            $table->renameColumn('service_charge_id', 'services_charge_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_services_charge', function (Blueprint $table) {
            $table->renameColumn('services_charge_id', 'service_charge_id');
        });
    }
}
