<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropDiscountRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('discount_requests', function (Blueprint $table) {
            $table->dropForeign('discount_requests_services_charge_id_foreign');
            $table->dropForeign('discount_requests_services_discount_id_foreign');
        });
        Schema::dropIfExists('discount_requests');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('discount_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('services_charge_id')->unsigned();
            $table->foreign('services_charge_id')
                  ->references('id')->on('services_charges')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->integer('services_discount_id')->unsigned();
            $table->foreign('services_discount_id')
                  ->references('id')->on('services_discounts')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->boolean('approval');
            $table->timestamps();

            
        });
    }
}
