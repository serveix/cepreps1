<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*SERVICIO 1*/
         DB::table('services')->insert([
            'name' => 'ESPIROMETRÍA',
            'price_k' => '812.70',
            'price_c' => '1150.00',
            'price_profesor' => '805.00',
        ]);
        
        /*SERVICIO 2*/
        DB::table('services')->insert([
            'name' => 'PLETISMOGRAFÍA',
            'price_k' => '2392.32',
            'price_c' => '3900.00',
            'price_profesor' => '2730.00',
        ]);

        /*SERVICIO 3*/
        DB::table('services')->insert([
            'name' => 'DIFUSIÓN DE MONOXIDO DE CARBONO (DLCO)',
            'price_k' => '4018.56',
            'price_c' => '5000.00',
            'price_profesor' => '3500.00',
        ]);

        /*SERVICIO 4*/
        DB::table('services')->insert([
            'name' => 'PRESIONES MÁXIMAS INSPIRATORIAS Y ESPIRATORIA (MIP-MEP)',
            'price_k' => '757.68',
            'price_c' => '750.00',
            'price_profesor' => '525.00',
        ]);

        /*SERVICIO 5*/
        DB::table('services')->insert([
            'name' => 'PBA. DE EJERCICIO CARDIOPULMONAR (CON GASES ESPIRADOS Y LINEA A.R.)',
            'price_k' => '5197.50',
            'price_c' => '6000.00',
            'price_profesor' => '4200.00',
        ]);

        /*SERVICIO 6*/
        DB::table('services')->insert([
            'name' => 'PBA. DE RETO S/G.E. SIN LINEA ARTERIAL',
            'price_k' => '2200.00',
            'price_c' => '2200.00',
            'price_profesor' => '1540.00',
        ]);

        /*SERVICIO 7*/
        DB::table('services')->insert([
            'name' => 'PBA. DE RETO S/G.E. CON LINEA ARTERIAL',
            'price_k' => '4332.72',
            'price_c' => '3700.00',
            'price_profesor' => '2590.00',
        ]);

        /*SERVICIO 8*/
        DB::table('services')->insert([
            'name' => 'PBA. DE RETO C/G.E. CON LINEA ARTERIAL',
            'price_k' => '5388.50',
            'price_c' => '6000.00' ,
            'price_profesor' => '4200.00',
        ]);

        /*SERVICIO 9*/
        DB::table('services')->insert([
            'name' => 'GASES ARTERIALES',
            'price_k' => '800.00',
            'price_c' => '800.00',
            'price_profesor' => '800.00',
        ]);

        /*SERVICIO 10*/
        DB::table('services')->insert([
            'name' => 'GASES VENOSOS',
            'price_k' => '800.00',
            'price_c' => '800.00',
            'price_profesor' => '800.00',
        ]);

        /*SERVICIO 11*/
        DB::table('services')->insert([
            'name' => 'CAMINATA 6 MINUTOS',
            'price_k' => '646.80',
            'price_c' => '646.80',
            'price_profesor' => '452.76',
        ]);

        /*SERVICIO 12*/
        DB::table('services')->insert([
            'name' => 'PROGRAMA DE REHABILITACIÓN PULMONAR (8 SEMANAS)',
            'price_k' => '8400.00',
            'price_c' => '8400.000',
            'price_profesor' => '8400.00',
        ]);

        /*SERVICIO 13*/
        DB::table('services')->insert([
            'name' => 'POLISOMNOGRAFÍA SIMPLE',
            'price_k' => '5197.50',
            'price_c' => '5197.50',
            'price_profesor' => '5197.50',
        ]);

        /*SERVICIO 14*/
        DB::table('services')->insert([
            'name' => 'POLISOMNOGRAFÍA CPAP',
            'price_k' => '4042.50',
            'price_c' => '4042.50',
            'price_profesor' => '4042.50',
        ]);

        /*SERVICIO 15*/
        DB::table('services')->insert([
            'name' => 'CONSULTA ESPECIALISTA NEUMÓLOGO',
            'price_k' => '840.00',
            'price_c' => '840.00',
            'price_profesor' => '840.00',
        ]);

        /*SERVICIO 15*/
        DB::table('services')->insert([
            'name' => 'CONSULTA ESPECIALISTA CARDIÓLOGO',
            'price_k' => '840.00',
            'price_c' => '840.00',
            'price_profesor' => '840.00',
        ]);
    }
}
