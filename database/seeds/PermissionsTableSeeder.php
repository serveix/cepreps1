<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            'value' => 'ADMIN_USERS',
        ]);

        DB::table('permissions')->insert([
            'value' => 'ADMIN_SERVICES',
        ]);

        DB::table('permissions')->insert([
            'value' => 'ADMIN_APPROVALS',
        ]);

        DB::table('permissions')->insert([
            'value' => 'ADMIN_CANCEL_RECEIPTS',
        ]);

        DB::table('permissions')->insert([
            'value' => 'ADMIN_RECEIPTS',
        ]);
    }
}
